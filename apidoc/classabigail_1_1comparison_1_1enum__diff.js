var classabigail_1_1comparison_1_1enum__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1enum__diff_1_1priv.html", null ],
    [ "enum_diff", "classabigail_1_1comparison_1_1enum__diff.html#a6c8450a3213bf9bb5c9e14d299e8d54b", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1enum__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "changed_enumerators", "classabigail_1_1comparison_1_1enum__diff.html#a8bdc95967c88db00cd1aca72db3de752", null ],
    [ "deleted_enumerators", "classabigail_1_1comparison_1_1enum__diff.html#ae2ece3116e866d5e39592dcdacf0d2f0", null ],
    [ "first_enum", "classabigail_1_1comparison_1_1enum__diff.html#add786130970352ec65bf3730f31b30e9", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1enum__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1enum__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1enum__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "inserted_enumerators", "classabigail_1_1comparison_1_1enum__diff.html#af6ba36d91f7d9608832bf4b0af4ef434", null ],
    [ "report", "classabigail_1_1comparison_1_1enum__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_enum", "classabigail_1_1comparison_1_1enum__diff.html#aece6c7aee771075be7937d2bd908280b", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1enum__diff.html#a822e810dfbd6af0b25017a5fe7d494a4", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1enum__diff.html#acdfef83cdc5838e8fbab269efb4fe6a1", null ]
];