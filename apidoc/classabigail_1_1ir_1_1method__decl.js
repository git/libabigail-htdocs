var classabigail_1_1ir_1_1method__decl =
[
    [ "method_decl", "classabigail_1_1ir_1_1method__decl.html#aba63018c81e84fabd0b30dceed676008", null ],
    [ "method_decl", "classabigail_1_1ir_1_1method__decl.html#a152cdb226ece5f1ac5f9453e8fc4ceac", null ],
    [ "method_decl", "classabigail_1_1ir_1_1method__decl.html#aac1498e0860b66e21659772e939d9227", null ],
    [ "get_type", "classabigail_1_1ir_1_1method__decl.html#ac9f2d8306c630d6621ce727e1f08ba35", null ],
    [ "set_linkage_name", "classabigail_1_1ir_1_1method__decl.html#a4360a75c92df4b3a9e5967509dc6780e", null ],
    [ "get_member_function_is_const", "classabigail_1_1ir_1_1method__decl.html#af5c9f6a9ffbe33d7e7a42779a53c9696", null ],
    [ "get_member_function_is_ctor", "classabigail_1_1ir_1_1method__decl.html#a9e85fa88032899a2947910469fc7fccc", null ],
    [ "get_member_function_is_dtor", "classabigail_1_1ir_1_1method__decl.html#ae0a196f41899a8f508f213a1c5a12288", null ],
    [ "get_member_function_is_virtual", "classabigail_1_1ir_1_1method__decl.html#af5eb1f7350d7de65b9d5935165aeb99c", null ],
    [ "get_member_function_vtable_offset", "classabigail_1_1ir_1_1method__decl.html#a6ac6aed87c04b6a359b652c34a82b852", null ],
    [ "member_function_has_vtable_offset", "classabigail_1_1ir_1_1method__decl.html#ade1efa191f74b78b5c43a0051d3168fb", null ],
    [ "set_member_function_is_const", "classabigail_1_1ir_1_1method__decl.html#ad6194bcc41e2544d3752078be68405f2", null ],
    [ "set_member_function_is_const", "classabigail_1_1ir_1_1method__decl.html#a3ab9ae01c3d7f54b74998a14401d994c", null ],
    [ "set_member_function_is_ctor", "classabigail_1_1ir_1_1method__decl.html#ab294b7f482f4682b0d12c59f865e0cb9", null ],
    [ "set_member_function_is_ctor", "classabigail_1_1ir_1_1method__decl.html#a60616f412429ad3054acac49bb481296", null ],
    [ "set_member_function_is_dtor", "classabigail_1_1ir_1_1method__decl.html#a443f60040bf3565e641edd49be012fae", null ],
    [ "set_member_function_is_dtor", "classabigail_1_1ir_1_1method__decl.html#aa359f0662c52679f9559bcf3d46eafe9", null ],
    [ "set_member_function_is_virtual", "classabigail_1_1ir_1_1method__decl.html#a4fe48cee1d87eaec7aa88f8475b7e35f", null ],
    [ "set_member_function_vtable_offset", "classabigail_1_1ir_1_1method__decl.html#abbe5f0347598bc9485f6f9cee015a7cf", null ],
    [ "set_member_function_vtable_offset", "classabigail_1_1ir_1_1method__decl.html#acfb449f3401cf21949a78f527d430fae", null ]
];