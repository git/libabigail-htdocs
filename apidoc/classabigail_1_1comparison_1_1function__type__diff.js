var classabigail_1_1comparison_1_1function__type__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1function__type__diff_1_1priv.html", null ],
    [ "function_type_diff", "classabigail_1_1comparison_1_1function__type__diff.html#acb6f7638a0d3da892a29f5cb2edebbe8", null ],
    [ "added_parms", "classabigail_1_1comparison_1_1function__type__diff.html#a744cdbf1f391ce2e3a8cf3c6ff7c3e69", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1function__type__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_function_type", "classabigail_1_1comparison_1_1function__type__diff.html#a963a5cbc7278e8cc20cc187f7dbba6be", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1function__type__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1function__type__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1function__type__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "removed_parms", "classabigail_1_1comparison_1_1function__type__diff.html#a702fc6f395bd824e28927ff67f20efca", null ],
    [ "report", "classabigail_1_1comparison_1_1function__type__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "return_type_diff", "classabigail_1_1comparison_1_1function__type__diff.html#a412f5dd413493f4c82b9bcafb316ee55", null ],
    [ "second_function_type", "classabigail_1_1comparison_1_1function__type__diff.html#a9c78bd1ad984f2b61496a58e677e48ff", null ],
    [ "sorted_added_parms", "classabigail_1_1comparison_1_1function__type__diff.html#a85896b1b681a8e072333d78936331d1f", null ],
    [ "sorted_deleted_parms", "classabigail_1_1comparison_1_1function__type__diff.html#af92d34621786cb55a68aa3bf3b6e4b1d", null ],
    [ "subtype_changed_parms", "classabigail_1_1comparison_1_1function__type__diff.html#a3761ad9c26525e2b86f1c883d82d904a", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1function__type__diff.html#a74b236659acbbd37ecc3b9145f1d4f54", null ]
];