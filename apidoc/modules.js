var modules =
[
    [ "Canonical diff tree nodes", "group___canonical_diff.html", null ],
    [ "Internal Representation of the comparison engine", "group___diff_node.html", null ],
    [ "Memory management", "group___memory.html", null ],
    [ "On-the-fly Canonicalization", "group___on_the_fly_canonicalization.html", null ],
    [ "Worker Threads", "group__thread__pool.html", null ]
];