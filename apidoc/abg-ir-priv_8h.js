var abg_ir_priv_8h =
[
    [ "class_or_union::priv", "structabigail_1_1ir_1_1class__or__union_1_1priv.html", "structabigail_1_1ir_1_1class__or__union_1_1priv" ],
    [ "environment::priv", "structabigail_1_1ir_1_1environment_1_1priv.html", "structabigail_1_1ir_1_1environment_1_1priv" ],
    [ "function_type::priv", "structabigail_1_1ir_1_1function__type_1_1priv.html", "structabigail_1_1ir_1_1function__type_1_1priv" ],
    [ "integral_type", "classabigail_1_1ir_1_1integral__type.html", "classabigail_1_1ir_1_1integral__type" ],
    [ "translation_unit::priv", "structabigail_1_1ir_1_1translation__unit_1_1priv.html", null ],
    [ "type_base::priv", "structabigail_1_1ir_1_1type__base_1_1priv.html", "structabigail_1_1ir_1_1type__base_1_1priv" ],
    [ "uint64_t_pair_hash", "structabigail_1_1ir_1_1uint64__t__pair__hash.html", "structabigail_1_1ir_1_1uint64__t__pair__hash" ],
    [ "type_comparison_result_type", "abg-ir-priv_8h.html#a141a2bb081cec67a1957d1b3cfa3f22c", null ],
    [ "uint64_t_pair_type", "abg-ir-priv_8h.html#af6548e1bd0a1c9b411baa069be1c2d65", null ],
    [ "uint64_t_pairs_set_type", "abg-ir-priv_8h.html#ad86b14c70aaeb4662768e9d88269d00f", null ],
    [ "comparison_result", "abg-ir-priv_8h.html#a3e72367e9acef4cedef80dbca77a5106", [
      [ "COMPARISON_RESULT_DIFFERENT", "abg-ir-priv_8h.html#a3e72367e9acef4cedef80dbca77a5106a79b54113d309a8935018affd88b57314", null ],
      [ "COMPARISON_RESULT_EQUAL", "abg-ir-priv_8h.html#a3e72367e9acef4cedef80dbca77a5106a23429725eab27d73be655901b44b255c", null ],
      [ "COMPARISON_RESULT_CYCLE_DETECTED", "abg-ir-priv_8h.html#a3e72367e9acef4cedef80dbca77a5106a4c5291df1e980a6eca1677b68ca3093e", null ],
      [ "COMPARISON_RESULT_UNKNOWN", "abg-ir-priv_8h.html#a3e72367e9acef4cedef80dbca77a5106a3c5022c26cc56ff3cc74d1d0b90b3997", null ]
    ] ],
    [ "operator&", "abg-ir-priv_8h.html#ac2eb843f3e0bb0a4d88834e7eb1a8d96", null ],
    [ "operator&=", "abg-ir-priv_8h.html#a900b778757c3276009489e7b020ad15b", null ],
    [ "operator|", "abg-ir-priv_8h.html#a043fca582464570d5f637e1519a91c9c", null ],
    [ "operator|=", "abg-ir-priv_8h.html#ac4f792fb84d68b0b0dd36ef1a8dcc168", null ],
    [ "operator~", "abg-ir-priv_8h.html#a2faa6c23881a12707a0e8aa8177d1eff", null ],
    [ "parse_integral_type", "abg-ir-priv_8h.html#a7d6604d399132fb70c1b40e76b4b8bba", null ]
];