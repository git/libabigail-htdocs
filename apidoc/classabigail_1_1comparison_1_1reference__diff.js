var classabigail_1_1comparison_1_1reference__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1reference__diff_1_1priv.html", null ],
    [ "reference_diff", "classabigail_1_1comparison_1_1reference__diff.html#ac942564745dab8096c72ab220964bbad", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1reference__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_reference", "classabigail_1_1comparison_1_1reference__diff.html#a144bd9fcc3c24c84f0b865859b3cd158", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1reference__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1reference__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1reference__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1reference__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_reference", "classabigail_1_1comparison_1_1reference__diff.html#a431c875c6afd935fc0e417a103283247", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1reference__diff.html#af70f896e003c0fc234be4c9b2c0d757a", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1reference__diff.html#adb11339d49c06376d8d472e54482a9bf", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1reference__diff.html#addc99236e8b0e7b8cf77036e337a19ea", null ]
];