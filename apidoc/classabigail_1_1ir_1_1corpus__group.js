var classabigail_1_1ir_1_1corpus__group =
[
    [ "corpus_group", "classabigail_1_1ir_1_1corpus__group.html#a57762c46e3b7646c517a42940a8e5ec6", null ],
    [ "~corpus_group", "classabigail_1_1ir_1_1corpus__group.html#ab49ed1fedcd8a2e442a230f8861ff4a4", null ],
    [ "add_corpus", "classabigail_1_1ir_1_1corpus__group.html#ac3fd08c3a7096af4682e711581be248e", null ],
    [ "get_corpora", "classabigail_1_1ir_1_1corpus__group.html#a1c7c9f489634b3502754e06826e29e8b", null ],
    [ "get_fun_symbol_map", "classabigail_1_1ir_1_1corpus__group.html#a7ca17329da42867fbb6b3f8fcc722364", null ],
    [ "get_functions", "classabigail_1_1ir_1_1corpus__group.html#af4a026e15004fcaa1307b161878fae2d", null ],
    [ "get_main_corpus", "classabigail_1_1ir_1_1corpus__group.html#a052b8f1ee9667f42f49334ead7db9666", null ],
    [ "get_main_corpus", "classabigail_1_1ir_1_1corpus__group.html#a8b1f17e9deb2b68c09537ff35963fcd0", null ],
    [ "get_public_types_pretty_representations", "classabigail_1_1ir_1_1corpus__group.html#a236be40bc92471b7d11a6733c94790b7", null ],
    [ "get_sorted_fun_symbols", "classabigail_1_1ir_1_1corpus__group.html#a46e390f5b16f583b63df4a987fd2a442", null ],
    [ "get_sorted_var_symbols", "classabigail_1_1ir_1_1corpus__group.html#a04effa76f91111288c4da656c7034cf0", null ],
    [ "get_unreferenced_function_symbols", "classabigail_1_1ir_1_1corpus__group.html#a91bd025a7bb1e3790ea43544f12714bf", null ],
    [ "get_unreferenced_variable_symbols", "classabigail_1_1ir_1_1corpus__group.html#a9821be0e83d9b194a82a8869b3410898", null ],
    [ "get_var_symbol_map", "classabigail_1_1ir_1_1corpus__group.html#a30df94954aadd418e2b0fb0a27880834", null ],
    [ "get_variables", "classabigail_1_1ir_1_1corpus__group.html#aaf8554fe5ad752730b765bc59a678506", null ],
    [ "is_empty", "classabigail_1_1ir_1_1corpus__group.html#a5a2b7a58dc85678d08752945ff655362", null ],
    [ "recording_types_reachable_from_public_interface_supported", "classabigail_1_1ir_1_1corpus__group.html#aa77dfdae30f663f273c0035687f5695f", null ]
];