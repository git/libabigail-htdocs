var classabigail_1_1comparison_1_1corpus__diff =
[
    [ "diff_stats", "classabigail_1_1comparison_1_1corpus__diff_1_1diff__stats.html", "classabigail_1_1comparison_1_1corpus__diff_1_1diff__stats" ],
    [ "priv", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html", "structabigail_1_1comparison_1_1corpus__diff_1_1priv" ],
    [ "diff_stats_sptr", "classabigail_1_1comparison_1_1corpus__diff.html#ab79b12937d86def3f65912865c7daece", null ],
    [ "corpus_diff", "classabigail_1_1comparison_1_1corpus__diff.html#a5b2ca57048c319299ea967cb4b225622", null ],
    [ "added_functions", "classabigail_1_1comparison_1_1corpus__diff.html#a9e2b3a2465e23d611a11e142ccdda20d", null ],
    [ "added_unreachable_types", "classabigail_1_1comparison_1_1corpus__diff.html#af4b51859931f83e11ddcc03dfce9cd1b", null ],
    [ "added_unreachable_types_sorted", "classabigail_1_1comparison_1_1corpus__diff.html#aac290e868cb5935d1ee26aa105b594d7", null ],
    [ "added_unrefed_function_symbols", "classabigail_1_1comparison_1_1corpus__diff.html#a652d7f491389247deb87f2a1cdd59094", null ],
    [ "added_unrefed_variable_symbols", "classabigail_1_1comparison_1_1corpus__diff.html#ac127c2cbfdb1dd748a1441bc9f0d492e", null ],
    [ "added_variables", "classabigail_1_1comparison_1_1corpus__diff.html#a73a3f676016c50e57325260d959142ec", null ],
    [ "append_child_node", "classabigail_1_1comparison_1_1corpus__diff.html#aed6206eca9342f8ee119430438ffaed1", null ],
    [ "apply_filters_and_suppressions_before_reporting", "classabigail_1_1comparison_1_1corpus__diff.html#a9447df2aa44fe1a40f92b3b4016c4091", null ],
    [ "architecture_changed", "classabigail_1_1comparison_1_1corpus__diff.html#afe04b9ee048caa78e9aaecdbef99677e", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1corpus__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "changed_functions", "classabigail_1_1comparison_1_1corpus__diff.html#af4d8ce9350231561f4ea75359be3fc3b", null ],
    [ "changed_functions_sorted", "classabigail_1_1comparison_1_1corpus__diff.html#ac2164e6521c143be86ed13baac8dfdcd", null ],
    [ "changed_unreachable_types", "classabigail_1_1comparison_1_1corpus__diff.html#a3c72c315f905294d121c61888dd8c294", null ],
    [ "changed_unreachable_types_sorted", "classabigail_1_1comparison_1_1corpus__diff.html#a1247c7a8a578b6f50f0f2dcd843c17ad", null ],
    [ "changed_variables", "classabigail_1_1comparison_1_1corpus__diff.html#a9ae387dba0dadfef2daae03e1acd4b91", null ],
    [ "changed_variables_sorted", "classabigail_1_1comparison_1_1corpus__diff.html#a4662bd76f475cc4bc66cec9f7a2b445b", null ],
    [ "children_nodes", "classabigail_1_1comparison_1_1corpus__diff.html#a3b04ef3c86906caad978c0ee0a699cc8", null ],
    [ "context", "classabigail_1_1comparison_1_1corpus__diff.html#a8cd1a057e116d846cd2dc53e3339291c", null ],
    [ "deleted_functions", "classabigail_1_1comparison_1_1corpus__diff.html#ae1143cf570d234f17a5c44cffb265f1c", null ],
    [ "deleted_unreachable_types", "classabigail_1_1comparison_1_1corpus__diff.html#aa4fd178221ff3343fc460cb7ff336e3a", null ],
    [ "deleted_unreachable_types_sorted", "classabigail_1_1comparison_1_1corpus__diff.html#a23e5bcfb22ecaece86cf39f3952e0b2b", null ],
    [ "deleted_unrefed_function_symbols", "classabigail_1_1comparison_1_1corpus__diff.html#a505b0cb6735a0861cc08dca0eebf64a0", null ],
    [ "deleted_unrefed_variable_symbols", "classabigail_1_1comparison_1_1corpus__diff.html#a53bd27b875d4d56bdd8d6ebcd984c8b0", null ],
    [ "deleted_variables", "classabigail_1_1comparison_1_1corpus__diff.html#a10a8129747392d809a2e66e739565c98", null ],
    [ "finish_diff_type", "classabigail_1_1comparison_1_1corpus__diff.html#a078b3245dc6aa69a1ce8a12c6ca0b781", null ],
    [ "first_corpus", "classabigail_1_1comparison_1_1corpus__diff.html#aac21248cea69f9b30b90c117f69080b2", null ],
    [ "function_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a35c33415c543224899ae75a4ba2494a6", null ],
    [ "get_leaf_diffs", "classabigail_1_1comparison_1_1corpus__diff.html#a77e44308127e1b29e7abd2ec9590c398", null ],
    [ "get_leaf_diffs", "classabigail_1_1comparison_1_1corpus__diff.html#a256009c4ecfff908274d6c5384f2905b", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1corpus__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_incompatible_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a00d3cf96900031012d9849eddf75b528", null ],
    [ "has_net_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a8e5f8f8bc6515f66dba0d8361c387960", null ],
    [ "has_net_subtype_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a71a082e663a30d2caa66b1f3e83b3d0e", null ],
    [ "mark_leaf_diff_nodes", "classabigail_1_1comparison_1_1corpus__diff.html#aa246a42fbfac760ae0f1d6571cd3a16b", null ],
    [ "report", "classabigail_1_1comparison_1_1corpus__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second_corpus", "classabigail_1_1comparison_1_1corpus__diff.html#a5ec1b66114d991b4132ddb787da1096d", null ],
    [ "soname_changed", "classabigail_1_1comparison_1_1corpus__diff.html#a2063dd9a34e7f5e958b0a3e806554314", null ],
    [ "traverse", "classabigail_1_1comparison_1_1corpus__diff.html#a92b8737eee70b8b8197902d6ae8fa0df", null ],
    [ "variable_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a37a69d67f45c59139a129b088331a304", null ],
    [ "apply_suppressions", "classabigail_1_1comparison_1_1corpus__diff.html#aefb1bee43aa6292e2f7c41bff3f2d327", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1corpus__diff.html#a224f7140d332b979ddbef94b55c8fe9f", null ],
    [ "maybe_report_unreachable_type_changes", "classabigail_1_1comparison_1_1corpus__diff.html#a3afdccf8bbbd20bb886986f97ea01cf1", null ]
];