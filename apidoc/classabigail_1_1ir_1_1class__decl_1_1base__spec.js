var classabigail_1_1ir_1_1class__decl_1_1base__spec =
[
    [ "hash", "structabigail_1_1ir_1_1class__decl_1_1base__spec_1_1hash.html", null ],
    [ "base_spec", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#aa413a978810c2478781b088631f8f1df", null ],
    [ "base_spec", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a509ff6299e9e0fb672d22436aa1f0cef", null ],
    [ "get_base_class", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a0d3a6473a4b306092ff3b160230ff99f", null ],
    [ "get_hash", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#ae1ac2018a4646d71125fa5473670aea8", null ],
    [ "get_is_virtual", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a1a136625ab9edbfd32a3ee3ecbf4cdf1", null ],
    [ "get_offset_in_bits", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a4d13c5ed6eda50612556f6ff294c3b46", null ],
    [ "operator==", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a1b18e87a47c596d122987d9e1ba95816", null ],
    [ "traverse", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html#a5f12d4bece91d0d613fc189bbca62113", null ]
];