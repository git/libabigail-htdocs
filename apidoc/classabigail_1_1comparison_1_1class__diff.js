var classabigail_1_1comparison_1_1class__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1class__diff_1_1priv.html", "structabigail_1_1comparison_1_1class__diff_1_1priv" ],
    [ "class_diff", "classabigail_1_1comparison_1_1class__diff.html#ade9516e981a8ea1127e44a766550c963", null ],
    [ "base_changes", "classabigail_1_1comparison_1_1class__diff.html#aae5cb9a9b1ebdc7e5f9a39dcc96fbcae", null ],
    [ "base_changes", "classabigail_1_1comparison_1_1class__diff.html#ad0d927bb0095429d3d03b213eba7db77", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1class__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "changed_bases", "classabigail_1_1comparison_1_1class__diff.html#a6c2f978053463ca1cd0350cd12b12508", null ],
    [ "deleted_bases", "classabigail_1_1comparison_1_1class__diff.html#aa08448ca0c8936215e3068465f6e8af1", null ],
    [ "first_class_decl", "classabigail_1_1comparison_1_1class__diff.html#a4b9b251c63b2974d18dde22631d44494", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1class__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1class__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1class__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "inserted_bases", "classabigail_1_1comparison_1_1class__diff.html#a87746c44d901b9c604ba556c0cde9a9c", null ],
    [ "moved_bases", "classabigail_1_1comparison_1_1class__diff.html#a782bd89492747a00f3794bd7ef3f73f2", null ],
    [ "report", "classabigail_1_1comparison_1_1class__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_class_decl", "classabigail_1_1comparison_1_1class__diff.html#aed105ba81a2bb2834374108695c3b29d", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1class__diff.html#aeaef46c14c92490c9a446ae94feba1ba", null ]
];