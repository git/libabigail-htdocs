var abg_libxml_utils_8cc =
[
    [ "build_sptr< xmlChar >", "abg-libxml-utils_8cc.html#a0a77b2de475b0955aa5493aec2edb33f", null ],
    [ "build_sptr< xmlTextReader >", "abg-libxml-utils_8cc.html#a7af3cf096fd2b3cfa00294d8dcb6e302", null ],
    [ "escape_xml_comment", "abg-libxml-utils_8cc.html#a7ccf7c8e8f196f86ffd835cd1b7c7a3e", null ],
    [ "escape_xml_comment", "abg-libxml-utils_8cc.html#a979e71a86fc7e695b97d4ed2d20096c2", null ],
    [ "escape_xml_string", "abg-libxml-utils_8cc.html#ae88db0dab03125da9e61a054566d8c4a", null ],
    [ "escape_xml_string", "abg-libxml-utils_8cc.html#af503406feedd594dabf0c6c37973954f", null ],
    [ "get_xml_node_depth", "abg-libxml-utils_8cc.html#a5bd10e754046bd78a89358f061ec2aa0", null ],
    [ "new_reader_from_buffer", "abg-libxml-utils_8cc.html#a9fc78238e4547847b3dc2b41c129e66a", null ],
    [ "new_reader_from_file", "abg-libxml-utils_8cc.html#a2055f635a5577d710e53555d0020e5a4", null ],
    [ "new_reader_from_istream", "abg-libxml-utils_8cc.html#ac4b5a6d9c2463cdfb48da02534e77909", null ],
    [ "unescape_xml_comment", "abg-libxml-utils_8cc.html#a14d45e63a222d35914dfa8419e5970f0", null ],
    [ "unescape_xml_comment", "abg-libxml-utils_8cc.html#a05ec09cb3c6c85fcc701b3ece4c21cfd", null ],
    [ "unescape_xml_string", "abg-libxml-utils_8cc.html#a865f2e1957db728e9831dfbc749c3a2b", null ],
    [ "unescape_xml_string", "abg-libxml-utils_8cc.html#abd31016ec8870db062e75b8507920e61", null ],
    [ "xml_char_sptr_to_string", "abg-libxml-utils_8cc.html#a613139a0357f637a1883eb4c18bdfe2e", null ]
];