var classabigail_1_1comparison_1_1var__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1var__diff_1_1priv.html", null ],
    [ "var_diff", "classabigail_1_1comparison_1_1var__diff.html#a6372b077e06c0374d0f28e7d6e9d61bc", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1var__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_var", "classabigail_1_1comparison_1_1var__diff.html#ab035fb7ca13ae3ce3806f674e9763cff", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1var__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1var__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1var__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1var__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second_var", "classabigail_1_1comparison_1_1var__diff.html#afb29aaf6f0feb9992c12eb832e5d0165", null ],
    [ "type_diff", "classabigail_1_1comparison_1_1var__diff.html#afaeac73d7326c056a2a9e9cc8f1ae132", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1var__diff.html#adb0b19b8bf54d9e3634892ae4187c036", null ]
];