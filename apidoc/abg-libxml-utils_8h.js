var abg_libxml_utils_8h =
[
    [ "charDeleter", "structabigail_1_1xml_1_1char_deleter.html", null ],
    [ "textReaderDeleter", "structabigail_1_1xml_1_1text_reader_deleter.html", null ],
    [ "XML_NODE_GET_ATTRIBUTE", "abg-libxml-utils_8h.html#aaac0e26ae21bafd95279b9351afecc7d", null ],
    [ "XML_READER_GET_ATTRIBUTE", "abg-libxml-utils_8h.html#a88cdd08e0474eedf783fbd3e0afe5a05", null ],
    [ "XML_READER_GET_NODE_NAME", "abg-libxml-utils_8h.html#a35dad510e7797a5ccc6bd8947f799cb2", null ],
    [ "XML_READER_GET_NODE_TYPE", "abg-libxml-utils_8h.html#ab662c303285e0d6e5ee92ea3c2be6295", null ],
    [ "reader_sptr", "abg-libxml-utils_8h.html#aeeeef7bfc21fbf43ec6bd9c8d1980c01", null ],
    [ "xml_char_sptr", "abg-libxml-utils_8h.html#ab5386c5aff8ab8154109a3f8911d10a5", null ],
    [ "build_sptr< xmlChar >", "abg-libxml-utils_8h.html#a331877994c5e569f4e9da55145e4dfe4", null ],
    [ "build_sptr< xmlTextReader >", "abg-libxml-utils_8h.html#af6aa1655cd4f7df21fc97194cacee5e2", null ],
    [ "escape_xml_comment", "abg-libxml-utils_8h.html#a7ccf7c8e8f196f86ffd835cd1b7c7a3e", null ],
    [ "escape_xml_comment", "abg-libxml-utils_8h.html#a979e71a86fc7e695b97d4ed2d20096c2", null ],
    [ "escape_xml_string", "abg-libxml-utils_8h.html#ae88db0dab03125da9e61a054566d8c4a", null ],
    [ "escape_xml_string", "abg-libxml-utils_8h.html#af503406feedd594dabf0c6c37973954f", null ],
    [ "get_xml_node_depth", "abg-libxml-utils_8h.html#a5bd10e754046bd78a89358f061ec2aa0", null ],
    [ "new_reader_from_buffer", "abg-libxml-utils_8h.html#a9fc78238e4547847b3dc2b41c129e66a", null ],
    [ "new_reader_from_file", "abg-libxml-utils_8h.html#a2055f635a5577d710e53555d0020e5a4", null ],
    [ "new_reader_from_istream", "abg-libxml-utils_8h.html#ac4b5a6d9c2463cdfb48da02534e77909", null ],
    [ "unescape_xml_comment", "abg-libxml-utils_8h.html#a14d45e63a222d35914dfa8419e5970f0", null ],
    [ "unescape_xml_comment", "abg-libxml-utils_8h.html#a05ec09cb3c6c85fcc701b3ece4c21cfd", null ],
    [ "unescape_xml_string", "abg-libxml-utils_8h.html#a865f2e1957db728e9831dfbc749c3a2b", null ],
    [ "unescape_xml_string", "abg-libxml-utils_8h.html#abd31016ec8870db062e75b8507920e61", null ],
    [ "xml_char_sptr_to_string", "abg-libxml-utils_8h.html#a613139a0357f637a1883eb4c18bdfe2e", null ]
];