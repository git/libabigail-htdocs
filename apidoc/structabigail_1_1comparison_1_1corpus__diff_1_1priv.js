var structabigail_1_1comparison_1_1corpus__diff_1_1priv =
[
    [ "priv", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a22cfc83b1998034a1cd85ce7fc933332", null ],
    [ "priv", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#aadacdd4fb0e89bd74f4d3e1336202f0f", null ],
    [ "added_function_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#af6cc14b90230d19c82127b9060d7dc7c", null ],
    [ "added_unreachable_type_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a058ac9e33298fd7f5e64d41e2dec84b2", null ],
    [ "added_unrefed_fn_sym_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ad6373a83955fcfae04bcd20f88692ca9", null ],
    [ "added_unrefed_var_sym_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a533244327f09c98cc02e178f0d6e811d", null ],
    [ "added_variable_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a79a4138f60662c9ba0da10ab79f9e2b5", null ],
    [ "apply_filters_and_compute_diff_stats", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a75cfdc3a7c03cadf45d43465fa6ddd87", null ],
    [ "apply_supprs_to_added_removed_fns_vars_unreachable_types", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ae717e274b07a08143aece71751420803", null ],
    [ "categorize_redundant_changed_sub_nodes", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a3872d1617346f65f1401409e1f7c3695", null ],
    [ "changed_unreachable_types_sorted", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a1247c7a8a578b6f50f0f2dcd843c17ad", null ],
    [ "clear_lookup_tables", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#afe0455e1a8aba95f207f7d48fd329925", null ],
    [ "clear_redundancy_categorization", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ac1c6326a35b2b6a775af8c5915c8cbed", null ],
    [ "count_leaf_changes", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ac2e9064d10c16a6331771659f3622514", null ],
    [ "count_leaf_type_changes", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ac58318c654d24d872ee7cf68e1bed8a7", null ],
    [ "count_unreachable_types", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#abd055035ff215c4f4317847bb2592ab6", null ],
    [ "deleted_function_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ad9405beb23d9d2ae23b16f49f273cd78", null ],
    [ "deleted_unreachable_type_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a9694f87a23d3e49d88e9879e9f20e94a", null ],
    [ "deleted_unrefed_fn_sym_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a6fa6eda90ed7ed044004fe7e0771423d", null ],
    [ "deleted_unrefed_var_sym_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#aab77ee8e7d740c1392699184caa70909", null ],
    [ "deleted_variable_is_suppressed", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#aaaea78a922abd8e4d86a603170c57061", null ],
    [ "emit_diff_stats", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#af4878de78367f3a46add16ca2d07d86f", null ],
    [ "ensure_lookup_tables_populated", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a09f2c7f048619520ef9b8b83bd47ff8a", null ],
    [ "get_context", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a1429e5491ab0d3b421e60dcab1e9ae61", null ],
    [ "lookup_tables_empty", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#a6e283a28c05317977b865c8157806b9b", null ],
    [ "maybe_dump_diff_tree", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html#ab84f3cadb79fb54b5eaf1412908ad4f3", null ]
];