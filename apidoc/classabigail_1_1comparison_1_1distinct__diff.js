var classabigail_1_1comparison_1_1distinct__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1distinct__diff_1_1priv.html", null ],
    [ "distinct_diff", "classabigail_1_1comparison_1_1distinct__diff.html#aab1f128602d4c814735cca7670921178", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1distinct__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "compatible_child_diff", "classabigail_1_1comparison_1_1distinct__diff.html#a96c804640644c903b0588f8476bc2b8a", null ],
    [ "first", "classabigail_1_1comparison_1_1distinct__diff.html#acb87b938791fb41be575aabd195a422a", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1distinct__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1distinct__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1distinct__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1distinct__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second", "classabigail_1_1comparison_1_1distinct__diff.html#adbb48ec3573c617e48edacf26f09cfae", null ],
    [ "compute_diff_for_distinct_kinds", "classabigail_1_1comparison_1_1distinct__diff.html#a35379fff3663996b02eb11cf2003eeca", null ]
];