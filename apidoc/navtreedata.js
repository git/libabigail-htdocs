/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "libabigail", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"abg-comp-filter_8cc.html",
"abg-comparison_8h.html#a1b789625df3a1f5c6d724a3bd8eb45d5",
"abg-dwarf-reader_8cc.html#adbb0d8c2e4e5001e7fc3ef6b88807a21a299e6558822333273537fb279ba5db78",
"abg-fwd_8h.html#a49ab15b41760b2d91822044a9705b539",
"abg-fwd_8h.html#af0efd88c799bc7306156d8980e1f8ff6",
"abg-ir_8cc.html#a49ab15b41760b2d91822044a9705b539",
"abg-ir_8cc.html#ac05fadbfa4aa25e6727a8c6eecc76d2a",
"abg-ir_8h.html#ae489dda2a286cbff83b53f2da04826d5",
"abg-tools-utils_8h.html#a1721bbee58dc2fd2d934610d40e64197",
"classabigail_1_1comparison_1_1class__or__union__diff.html",
"classabigail_1_1comparison_1_1corpus__diff_1_1diff__stats.html#a8cad425229a7462b9f92af69125b6863",
"classabigail_1_1comparison_1_1diff__context.html#a4820a5b849a91de15e8787968603b0af",
"classabigail_1_1comparison_1_1enum__diff.html#add786130970352ec65bf3730f31b30e9",
"classabigail_1_1comparison_1_1typedef__diff.html#a02b9a216a743f23d9029a420b2a98742",
"classabigail_1_1ini_1_1config.html#a24a0a741209ef8af050080bd1f202a12",
"classabigail_1_1ir_1_1array__type__def_1_1subrange__type.html#aea2770952dab500dd4565fb7881411f3",
"classabigail_1_1ir_1_1corpus.html#a06dbc3a3eb5aaab80edf32ea607f1ef3afeebe31e1bb7b019be391f22136ce3de",
"classabigail_1_1ir_1_1decl__base.html",
"classabigail_1_1ir_1_1elf__symbol.html#aa59c24500ac556300d5ec9b138c423a9",
"classabigail_1_1ir_1_1function__type.html#a63bac6eb401369d4f0c4e444d1a1b6ea",
"classabigail_1_1ir_1_1non__type__tparameter.html",
"classabigail_1_1ir_1_1translation__unit.html#a7461af24f80b5297957b6de85293fdd2",
"classabigail_1_1ir_1_1type__or__decl__base.html#ab8da13b9e383c4d0712e33abab008691",
"classabigail_1_1suppr_1_1function__suppression.html#acf02d58362088e164a9b6701e320e795",
"classabigail_1_1suppr_1_1variable__suppression.html#a2f366e6a524fd06615df032fd86d2dc3",
"namespaceabigail.html#ac463c41c830c1af899103b24e49f3a2d",
"namespaceabigail_1_1comparison.html#aefb1bee43aa6292e2f7c41bff3f2d327",
"namespaceabigail_1_1ir.html#a28a01357eacd6fb3f661051b1a4ab559",
"namespaceabigail_1_1ir.html#a8a4af4916d90b36100a266c6b84e6996",
"namespaceabigail_1_1ir.html#afc9a0457b025f5a756daaf1727f87fda",
"namespacemembers_u.html",
"structabigail_1_1fe__iface_1_1options__type.html#a2dac7b4d36d1a9cd131c0ef099751c42",
"structabigail_1_1ir_1_1uint64__t__pair__hash.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';