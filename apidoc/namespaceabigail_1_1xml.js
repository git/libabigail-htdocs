var namespaceabigail_1_1xml =
[
    [ "charDeleter", "structabigail_1_1xml_1_1char_deleter.html", null ],
    [ "textReaderDeleter", "structabigail_1_1xml_1_1text_reader_deleter.html", null ],
    [ "reader_sptr", "namespaceabigail_1_1xml.html#aeeeef7bfc21fbf43ec6bd9c8d1980c01", null ],
    [ "xml_char_sptr", "namespaceabigail_1_1xml.html#ab5386c5aff8ab8154109a3f8911d10a5", null ],
    [ "escape_xml_comment", "namespaceabigail_1_1xml.html#a7ccf7c8e8f196f86ffd835cd1b7c7a3e", null ],
    [ "escape_xml_comment", "namespaceabigail_1_1xml.html#a979e71a86fc7e695b97d4ed2d20096c2", null ],
    [ "escape_xml_string", "namespaceabigail_1_1xml.html#ae88db0dab03125da9e61a054566d8c4a", null ],
    [ "escape_xml_string", "namespaceabigail_1_1xml.html#af503406feedd594dabf0c6c37973954f", null ],
    [ "get_xml_node_depth", "namespaceabigail_1_1xml.html#a5bd10e754046bd78a89358f061ec2aa0", null ],
    [ "new_reader_from_buffer", "namespaceabigail_1_1xml.html#a9fc78238e4547847b3dc2b41c129e66a", null ],
    [ "new_reader_from_file", "namespaceabigail_1_1xml.html#a2055f635a5577d710e53555d0020e5a4", null ],
    [ "new_reader_from_istream", "namespaceabigail_1_1xml.html#ac4b5a6d9c2463cdfb48da02534e77909", null ],
    [ "unescape_xml_comment", "namespaceabigail_1_1xml.html#a14d45e63a222d35914dfa8419e5970f0", null ],
    [ "unescape_xml_comment", "namespaceabigail_1_1xml.html#a05ec09cb3c6c85fcc701b3ece4c21cfd", null ],
    [ "unescape_xml_string", "namespaceabigail_1_1xml.html#a865f2e1957db728e9831dfbc749c3a2b", null ],
    [ "unescape_xml_string", "namespaceabigail_1_1xml.html#abd31016ec8870db062e75b8507920e61", null ],
    [ "xml_char_sptr_to_string", "namespaceabigail_1_1xml.html#a613139a0357f637a1883eb4c18bdfe2e", null ]
];