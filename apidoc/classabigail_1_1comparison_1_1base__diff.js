var classabigail_1_1comparison_1_1base__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1base__diff_1_1priv.html", null ],
    [ "base_diff", "classabigail_1_1comparison_1_1base__diff.html#a9d886f5f000ba6043ec6707051cb3e7c", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1base__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_base", "classabigail_1_1comparison_1_1base__diff.html#acb295024d33f42037a6c9e16ec38431c", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1base__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "get_underlying_class_diff", "classabigail_1_1comparison_1_1base__diff.html#aa3fd83da39a6bec1a6ea489c9bc498ce", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1base__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1base__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1base__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_base", "classabigail_1_1comparison_1_1base__diff.html#a994caef20e6427b7a8a3bd60965aa5ac", null ],
    [ "set_underlying_class_diff", "classabigail_1_1comparison_1_1base__diff.html#a6c3dc04bea3a1d66ab1e95352b790de3", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1base__diff.html#afbd16298ae1564bef21ae11238a4ec10", null ]
];