var classabigail_1_1ir_1_1enum__type__decl =
[
    [ "enumerator", "classabigail_1_1ir_1_1enum__type__decl_1_1enumerator.html", "classabigail_1_1ir_1_1enum__type__decl_1_1enumerator" ],
    [ "enumerators", "classabigail_1_1ir_1_1enum__type__decl.html#a4dedb8393999c52a4265aeb4639e665f", null ],
    [ "enum_type_decl", "classabigail_1_1ir_1_1enum__type__decl.html#a766372a23a9dfbd9016cd9fb86cb5b51", null ],
    [ "~enum_type_decl", "classabigail_1_1ir_1_1enum__type__decl.html#a632dd07c78f73cbfe5c0859d8991ec53", null ],
    [ "get_enumerators", "classabigail_1_1ir_1_1enum__type__decl.html#aae90ea1da82af753053a2b0b061b1ec5", null ],
    [ "get_enumerators", "classabigail_1_1ir_1_1enum__type__decl.html#a696b9e4b8a27df34214db938b3c8e424", null ],
    [ "get_pretty_representation", "classabigail_1_1ir_1_1enum__type__decl.html#aea2770952dab500dd4565fb7881411f3", null ],
    [ "get_underlying_type", "classabigail_1_1ir_1_1enum__type__decl.html#ab21b00c256823331fdbc78c9913ebdce", null ],
    [ "operator==", "classabigail_1_1ir_1_1enum__type__decl.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1enum__type__decl.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "traverse", "classabigail_1_1ir_1_1enum__type__decl.html#aa0435efbde239af376b5d0dfa16135d2", null ],
    [ "enum_has_non_name_change", "classabigail_1_1ir_1_1enum__type__decl.html#a0431523f6fdc242b6d140dd06c81023a", null ]
];