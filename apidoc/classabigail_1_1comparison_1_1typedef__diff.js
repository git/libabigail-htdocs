var classabigail_1_1comparison_1_1typedef__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1typedef__diff_1_1priv.html", null ],
    [ "typedef_diff", "classabigail_1_1comparison_1_1typedef__diff.html#ac398a8f4a1f8741827ae8d6fce65b23a", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1typedef__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_typedef_decl", "classabigail_1_1comparison_1_1typedef__diff.html#a36fa528d3ca150cfc09b2cb31d9b7cad", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1typedef__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1typedef__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1typedef__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1typedef__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_typedef_decl", "classabigail_1_1comparison_1_1typedef__diff.html#a27de221aaa482176aae3d1aa51d06cde", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1typedef__diff.html#ac39ece8847a4c6caaf30628d72152c69", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1typedef__diff.html#a54448223c9a6a5ccbfab3a7126059c72", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1typedef__diff.html#a02b9a216a743f23d9029a420b2a98742", null ]
];