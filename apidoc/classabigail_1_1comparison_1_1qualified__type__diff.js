var classabigail_1_1comparison_1_1qualified__type__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1qualified__type__diff_1_1priv.html", null ],
    [ "qualified_type_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html#a972c52bad1a8ee24da18d6d0623aad68", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1qualified__type__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_qualified_type", "classabigail_1_1comparison_1_1qualified__type__diff.html#ad1c2366c2a430a85ee746c99e988d74c", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1qualified__type__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1qualified__type__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1qualified__type__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "leaf_underlying_type_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html#a65f473da8ae294db8a70975f753e9f76", null ],
    [ "report", "classabigail_1_1comparison_1_1qualified__type__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_qualified_type", "classabigail_1_1comparison_1_1qualified__type__diff.html#ab79c6f84ea60814ec940f8a16471bc57", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html#a822e810dfbd6af0b25017a5fe7d494a4", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html#a54448223c9a6a5ccbfab3a7126059c72", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html#a64b1ae1f3bcbe4fa701ab9f1c11afa0f", null ]
];