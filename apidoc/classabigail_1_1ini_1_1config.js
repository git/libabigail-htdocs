var classabigail_1_1ini_1_1config =
[
    [ "section", "classabigail_1_1ini_1_1config_1_1section.html", "classabigail_1_1ini_1_1config_1_1section" ],
    [ "properties_type", "classabigail_1_1ini_1_1config.html#a0843784f9b3c79c0db2176391ea5b5cf", null ],
    [ "section_sptr", "classabigail_1_1ini_1_1config.html#a24a0a741209ef8af050080bd1f202a12", null ],
    [ "sections_type", "classabigail_1_1ini_1_1config.html#a4b3bb41bce118f86e8dbdcdadd1d34ec", null ],
    [ "config", "classabigail_1_1ini_1_1config.html#ab2a8d77c01cdeabc1e6aa7d4cc3178cf", null ],
    [ "get_path", "classabigail_1_1ini_1_1config.html#a615567927dd536bd77aca1c7c0bb14a9", null ],
    [ "get_sections", "classabigail_1_1ini_1_1config.html#a59261b2b929b161bb488aff6c8a4672c", null ],
    [ "set_path", "classabigail_1_1ini_1_1config.html#a399bde3ad6b5c3927bb6c9569f1e8a54", null ],
    [ "set_sections", "classabigail_1_1ini_1_1config.html#a454d6f7013082f7298f9d39ee781ada0", null ]
];