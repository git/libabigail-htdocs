var abg_reader_8cc =
[
    [ "add_reader_suppressions", "abg-reader_8cc.html#a1797e21e2efbb74529e99204a4fae704", null ],
    [ "consider_types_not_reachable_from_public_interfaces", "abg-reader_8cc.html#af6939da19d5721ebaafa07769a284ba2", null ],
    [ "create_reader", "abg-reader_8cc.html#aefa038c9c5d722b1b0c1124a9f20de95", null ],
    [ "create_reader", "abg-reader_8cc.html#a07497c1b80bf5c438c7d24518b25b187", null ],
    [ "read_corpus_from_abixml", "abg-reader_8cc.html#a7be456ea19cb9c9c2960a796fb660282", null ],
    [ "read_corpus_from_abixml_file", "abg-reader_8cc.html#a8c68ae64f311f19595fa72893e8087a9", null ],
    [ "read_corpus_group_from_abixml", "abg-reader_8cc.html#ac5aca01f86f7a239a2222862f2c5f12a", null ],
    [ "read_corpus_group_from_abixml_file", "abg-reader_8cc.html#afd14d5e590bc603d5138f368dd2a2d2c", null ],
    [ "read_corpus_group_from_input", "abg-reader_8cc.html#a8560b958fe8dd73a96a9a0d134f33e2c", null ],
    [ "read_translation_unit", "abg-reader_8cc.html#a2f703b5561d5c85a8336f41c4b7d751d", null ],
    [ "read_translation_unit_from_buffer", "abg-reader_8cc.html#a14d6c1d49a2cbda546f84f835c9c0c8c", null ],
    [ "read_translation_unit_from_file", "abg-reader_8cc.html#ab52fbcdea2fe7ab1eecdc6ca871716e5", null ],
    [ "read_translation_unit_from_istream", "abg-reader_8cc.html#a4fa6b86763a1d5e84a7e72cbe274829c", null ]
];