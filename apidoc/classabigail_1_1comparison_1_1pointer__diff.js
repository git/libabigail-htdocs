var classabigail_1_1comparison_1_1pointer__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1pointer__diff_1_1priv.html", null ],
    [ "pointer_diff", "classabigail_1_1comparison_1_1pointer__diff.html#a940954d9de621f8697927b5e4570426a", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1pointer__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_pointer", "classabigail_1_1comparison_1_1pointer__diff.html#a0b1b13c8b8d42c92b9918309f6dbd74e", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1pointer__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1pointer__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1pointer__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1pointer__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_pointer", "classabigail_1_1comparison_1_1pointer__diff.html#a1803714b776ed0cce25ab24eff72b827", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1pointer__diff.html#a822e810dfbd6af0b25017a5fe7d494a4", null ],
    [ "underlying_type_diff", "classabigail_1_1comparison_1_1pointer__diff.html#a54448223c9a6a5ccbfab3a7126059c72", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1pointer__diff.html#ae00dfc8360bd1d9e4d7d0fe083313b30", null ]
];