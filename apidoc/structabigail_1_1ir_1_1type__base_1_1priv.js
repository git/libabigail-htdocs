var structabigail_1_1ir_1_1type__base_1_1priv =
[
    [ "canonical_type_propagated", "structabigail_1_1ir_1_1type__base_1_1priv.html#aaf5fec1588ea6380258c3c18f9a8e59d", null ],
    [ "clear_propagated_canonical_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#a84b3579f620981bbab92be6cdf86df78", null ],
    [ "depends_on_recursive_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#addf4da7ae9e5f8b7ba738adbae04906b", null ],
    [ "depends_on_recursive_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#a772f5d4ae075336758787967f8b25006", null ],
    [ "propagated_canonical_type_confirmed", "structabigail_1_1ir_1_1type__base_1_1priv.html#a8661406bd9970300e88ae1475fb27df6", null ],
    [ "set_canonical_type_propagated", "structabigail_1_1ir_1_1type__base_1_1priv.html#ac8163fcb04522ac8b35f4f17283009ce", null ],
    [ "set_depends_on_recursive_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#aad15be72c3bca5fde7455cdee10a75ea", null ],
    [ "set_does_not_depend_on_recursive_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#a4d330e79a6b40db1f53fb2c90f4b3e0b", null ],
    [ "set_does_not_depend_on_recursive_type", "structabigail_1_1ir_1_1type__base_1_1priv.html#a02901659bdd82f79f885109f90562c67", null ],
    [ "set_propagated_canonical_type_confirmed", "structabigail_1_1ir_1_1type__base_1_1priv.html#a4309495450b7b840d1b60ffb6f5d2a7f", null ]
];