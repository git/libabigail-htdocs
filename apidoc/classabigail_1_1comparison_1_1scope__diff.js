var classabigail_1_1comparison_1_1scope__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1scope__diff_1_1priv.html", null ],
    [ "scope_diff", "classabigail_1_1comparison_1_1scope__diff.html#a8879d846415344beea530bb928071f55", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1scope__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "changed_decls", "classabigail_1_1comparison_1_1scope__diff.html#a6d8d2218387536e813bf8c46912fe591", null ],
    [ "changed_types", "classabigail_1_1comparison_1_1scope__diff.html#a18c0cfd68a60cd2615a65fb72e6ced0e", null ],
    [ "deleted_member_at", "classabigail_1_1comparison_1_1scope__diff.html#a67142078bdf1cdac57ce4116c528b34c", null ],
    [ "deleted_member_at", "classabigail_1_1comparison_1_1scope__diff.html#a4a6f883adebc548120d9b6c8091ef693", null ],
    [ "first_scope", "classabigail_1_1comparison_1_1scope__diff.html#a56c6238e384c6d4c2327366ae507bfbf", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1scope__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1scope__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1scope__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "inserted_member_at", "classabigail_1_1comparison_1_1scope__diff.html#aa745c08fb6742326e4cf2d2459f12191", null ],
    [ "inserted_member_at", "classabigail_1_1comparison_1_1scope__diff.html#af2c3440facb0f5f482efb5e94c340067", null ],
    [ "member_changes", "classabigail_1_1comparison_1_1scope__diff.html#a2e40140e4bbeca4af9bd50851582be22", null ],
    [ "member_changes", "classabigail_1_1comparison_1_1scope__diff.html#af9b36ca6aa06841b4f6d34dcc173d625", null ],
    [ "report", "classabigail_1_1comparison_1_1scope__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second_scope", "classabigail_1_1comparison_1_1scope__diff.html#a2e5f2fc2562eb6019e93b03e70644b9a", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1scope__diff.html#aa43955e3f2959495813804525d4485b3", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1scope__diff.html#a9af6ad0dc8f08c6fa65f9d58bc6b31ca", null ]
];