var classabigail_1_1comparison_1_1translation__unit__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1translation__unit__diff_1_1priv.html", null ],
    [ "translation_unit_diff", "classabigail_1_1comparison_1_1translation__unit__diff.html#a9278119b753cdd9c71688c85018c7d09", null ],
    [ "first_translation_unit", "classabigail_1_1comparison_1_1translation__unit__diff.html#adfc916290b0402ad21d41b843f909877", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1translation__unit__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1translation__unit__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1translation__unit__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second_translation_unit", "classabigail_1_1comparison_1_1translation__unit__diff.html#a0daba8b0f54a3672bbe19ff2a78c8000", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1translation__unit__diff.html#a10b3629c1c685fa5164b6e593d0140f6", null ]
];