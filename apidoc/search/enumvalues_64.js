var searchData=
[
  ['default_5ftimer_5fkind',['DEFAULT_TIMER_KIND',['../classabigail_1_1tools__utils_1_1timer.html#aaf25ee7e2306d78c74ec7bc48f092e81ac682fef42e84fce3d42618c024c564df',1,'abigail::tools_utils::timer']]],
  ['default_5fvisiting_5fkind',['DEFAULT_VISITING_KIND',['../namespaceabigail_1_1comparison.html#a77fa7cf7bdc2a3c0c40e6642dbab3f34a55ca7bbace8d923ef2f7a1b194d42095',1,'abigail::comparison']]],
  ['deleted_5ffunction_5fchange_5fkind',['DELETED_FUNCTION_CHANGE_KIND',['../classabigail_1_1suppr_1_1function__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a4a37612af00484252deda4653f8ed0c0',1,'abigail::suppr::function_suppression']]],
  ['deleted_5fvariable_5fchange_5fkind',['DELETED_VARIABLE_CHANGE_KIND',['../classabigail_1_1suppr_1_1variable__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a47facdab17751f27dd5080c9ed28864b',1,'abigail::suppr::variable_suppression']]],
  ['direct_5freach_5fkind',['DIRECT_REACH_KIND',['../classabigail_1_1suppr_1_1type__suppression.html#a86cd8e520de6c1ec6f6b3e3e05892a9daa865e3f08676890655ee63ed5ffc658d',1,'abigail::suppr::type_suppression']]],
  ['do_5fnot_5fmark_5fvisited_5fnodes_5fas_5fvisited',['DO_NOT_MARK_VISITED_NODES_AS_VISITED',['../namespaceabigail_1_1comparison.html#a77fa7cf7bdc2a3c0c40e6642dbab3f34ac11997c533558de2a36f21e02557c5d5',1,'abigail::comparison']]],
  ['double_5fbase_5ftype',['DOUBLE_BASE_TYPE',['../classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9ae58d3136a59c53d101bb90a6f1e6fa5b',1,'abigail::ir::integral_type']]]
];
