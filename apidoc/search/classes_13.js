var searchData=
[
  ['var_5fcomp_0',['var_comp',['../structabigail_1_1comparison_1_1var__comp.html',1,'abigail::comparison']]],
  ['var_5fdecl_1',['var_decl',['../classabigail_1_1ir_1_1var__decl.html',1,'abigail::ir']]],
  ['var_5fdiff_2',['var_diff',['../classabigail_1_1comparison_1_1var__diff.html',1,'abigail::comparison']]],
  ['var_5fdiff_5fsptr_5fcomp_3',['var_diff_sptr_comp',['../structabigail_1_1comparison_1_1var__diff__sptr__comp.html',1,'abigail::comparison']]],
  ['variable_5fsuppression_4',['variable_suppression',['../classabigail_1_1suppr_1_1variable__suppression.html',1,'abigail::suppr']]],
  ['version_5',['version',['../classabigail_1_1ir_1_1elf__symbol_1_1version.html',1,'abigail::ir::elf_symbol']]],
  ['virtual_5fmember_5ffunction_5fdiff_5fcomp_6',['virtual_member_function_diff_comp',['../structabigail_1_1comparison_1_1virtual__member__function__diff__comp.html',1,'abigail::comparison']]]
];
