var searchData=
[
  ['base_5fdiff_0',['base_diff',['../classabigail_1_1comparison_1_1base__diff.html',1,'abigail::comparison']]],
  ['base_5fdiff_5fcomp_1',['base_diff_comp',['../structabigail_1_1comparison_1_1base__diff__comp.html',1,'abigail::comparison']]],
  ['base_5fiterator_2',['base_iterator',['../classbase__iterator.html',1,'']]],
  ['base_5fspec_3',['base_spec',['../classabigail_1_1ir_1_1class__decl_1_1base__spec.html',1,'abigail::ir::class_decl']]],
  ['base_5fspec_5fcomp_4',['base_spec_comp',['../structabigail_1_1comparison_1_1base__spec__comp.html',1,'abigail::comparison']]],
  ['bound_5fvalue_5',['bound_value',['../classabigail_1_1ir_1_1array__type__def_1_1subrange__type_1_1bound__value.html',1,'abigail::ir::array_type_def::subrange_type']]],
  ['boundary_6',['boundary',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range_1_1boundary.html',1,'abigail::suppr::type_suppression::insertion_range']]]
];
