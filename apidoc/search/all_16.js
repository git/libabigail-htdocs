var searchData=
[
  ['xml_5fchar_5fsptr_0',['xml_char_sptr',['../namespaceabigail_1_1xml.html#ab5386c5aff8ab8154109a3f8911d10a5',1,'abigail::xml']]],
  ['xml_5fchar_5fsptr_5fto_5fstring_1',['xml_char_sptr_to_string',['../namespaceabigail_1_1xml.html#a613139a0357f637a1883eb4c18bdfe2e',1,'abigail::xml']]],
  ['xml_5fnode_5fget_5fattribute_2',['XML_NODE_GET_ATTRIBUTE',['../abg-libxml-utils_8h.html#aaac0e26ae21bafd95279b9351afecc7d',1,'abg-libxml-utils.h']]],
  ['xml_5freader_5fget_5fattribute_3',['XML_READER_GET_ATTRIBUTE',['../abg-libxml-utils_8h.html#a88cdd08e0474eedf783fbd3e0afe5a05',1,'abg-libxml-utils.h']]],
  ['xml_5freader_5fget_5fnode_5fname_4',['XML_READER_GET_NODE_NAME',['../abg-libxml-utils_8h.html#a35dad510e7797a5ccc6bd8947f799cb2',1,'abg-libxml-utils.h']]],
  ['xml_5freader_5fget_5fnode_5ftype_5',['XML_READER_GET_NODE_TYPE',['../abg-libxml-utils_8h.html#ab662c303285e0d6e5ee92ea3c2be6295',1,'abg-libxml-utils.h']]]
];
