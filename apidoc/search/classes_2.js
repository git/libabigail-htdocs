var searchData=
[
  ['canonical_5ftype_5fhash_0',['canonical_type_hash',['../structabigail_1_1ir_1_1canonical__type__hash.html',1,'abigail::ir']]],
  ['canvas_1',['canvas',['../structabigail_1_1canvas.html',1,'abigail']]],
  ['changed_5fenumerator_5fcomp_2',['changed_enumerator_comp',['../structabigail_1_1comparison_1_1changed__enumerator__comp.html',1,'abigail::comparison']]],
  ['chardeleter_3',['charDeleter',['../structabigail_1_1xml_1_1char_deleter.html',1,'abigail::xml']]],
  ['child_5fnode_4',['child_node',['../structabigail_1_1child__node.html',1,'abigail']]],
  ['class_5fdecl_5',['class_decl',['../classabigail_1_1ir_1_1class__decl.html',1,'abigail::ir']]],
  ['class_5fdiff_6',['class_diff',['../classabigail_1_1comparison_1_1class__diff.html',1,'abigail::comparison']]],
  ['class_5for_5funion_7',['class_or_union',['../classabigail_1_1ir_1_1class__or__union.html',1,'abigail::ir']]],
  ['class_5for_5funion_5fdiff_8',['class_or_union_diff',['../classabigail_1_1comparison_1_1class__or__union__diff.html',1,'abigail::comparison']]],
  ['class_5ftdecl_9',['class_tdecl',['../classabigail_1_1ir_1_1class__tdecl.html',1,'abigail::ir']]],
  ['config_10',['config',['../classabigail_1_1config.html',1,'config'],['../classabigail_1_1ini_1_1config.html',1,'config']]],
  ['context_5frel_11',['context_rel',['../classabigail_1_1ir_1_1context__rel.html',1,'abigail::ir']]],
  ['corpus_12',['corpus',['../classabigail_1_1ir_1_1corpus.html',1,'abigail::ir']]],
  ['corpus_5fdiff_13',['corpus_diff',['../classabigail_1_1comparison_1_1corpus__diff.html',1,'abigail::comparison']]],
  ['corpus_5fgroup_14',['corpus_group',['../classabigail_1_1ir_1_1corpus__group.html',1,'abigail::ir']]]
];
