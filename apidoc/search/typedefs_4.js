var searchData=
[
  ['elf_5fsymbol_5fsptr_0',['elf_symbol_sptr',['../namespaceabigail_1_1ir.html#a313302c342a903ff16cb46d8e457fef4',1,'abigail::ir']]],
  ['elf_5fsymbol_5fwptr_1',['elf_symbol_wptr',['../namespaceabigail_1_1ir.html#a9d1ea035b8e509086e02dbc26feb349e',1,'abigail::ir']]],
  ['elf_5fsymbols_2',['elf_symbols',['../namespaceabigail_1_1ir.html#a4ec7ed8654859e7f0888640b6a893c6a',1,'abigail::ir']]],
  ['enum_5ftype_5fdecl_5fsptr_3',['enum_type_decl_sptr',['../namespaceabigail_1_1ir.html#ab069e4c80c8cb856a85644bbeeb1fc5b',1,'abigail::ir']]],
  ['enumerators_4',['enumerators',['../classabigail_1_1ir_1_1enum__type__decl.html#a21b6b4248e06c9087ac7fa301152cb8e',1,'abigail::ir::enum_type_decl']]],
  ['enums_5ftype_5',['enums_type',['../namespaceabigail_1_1ir.html#af32af7b69d2d1d0c252036c3ddf65075',1,'abigail::ir']]],
  ['environment_5fsptr_6',['environment_sptr',['../namespaceabigail_1_1ir.html#a8b1f2783fa94954ff476bbde9bf9833a',1,'abigail::ir']]],
  ['exported_5fdecls_5fbuilder_5fsptr_7',['exported_decls_builder_sptr',['../classabigail_1_1ir_1_1corpus.html#aa06086658e7b24a6f86d5bf776a282b9',1,'abigail::ir::corpus']]]
];
