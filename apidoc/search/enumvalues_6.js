var searchData=
[
  ['harmless_5fdata_5fmember_5fchange_5fcategory_0',['HARMLESS_DATA_MEMBER_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a19d6dce0137d85d52a1d2566f68c1552',1,'abigail::comparison']]],
  ['harmless_5fdecl_5fname_5fchange_5fcategory_1',['HARMLESS_DECL_NAME_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a9db52e568ec9fe21f6874ae4aa4f72ca',1,'abigail::comparison']]],
  ['harmless_5fenum_5fchange_5fcategory_2',['HARMLESS_ENUM_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a65f2e4aeea430e233e943b97c4da4e4c',1,'abigail::comparison']]],
  ['harmless_5fsymbol_5falias_5fchange_5fcategory_3',['HARMLESS_SYMBOL_ALIAS_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9af7482eeb491dd890061b9566f8a4ae77',1,'abigail::comparison']]],
  ['harmless_5funion_5for_5fclass_5fchange_5fcategory_4',['HARMLESS_UNION_OR_CLASS_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a944241f2b1995323d73a631d3de9d28c',1,'abigail::comparison']]],
  ['has_5fallowed_5fchange_5fcategory_5',['HAS_ALLOWED_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9ae5463fdea121492b34bb66428540c542',1,'abigail::comparison']]],
  ['has_5fdescendant_5fwith_5fallowed_5fchange_5fcategory_6',['HAS_DESCENDANT_WITH_ALLOWED_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a63fa24addcc7ec3bdca6af6fa3226663',1,'abigail::comparison']]],
  ['has_5fparent_5fwith_5fallowed_5fchange_5fcategory_7',['HAS_PARENT_WITH_ALLOWED_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9abc7fa419cb73fecf454e5152906d1017',1,'abigail::comparison']]],
  ['hashing_5fcycled_5ftype_5fstate_8',['HASHING_CYCLED_TYPE_STATE',['../namespaceabigail_1_1hashing.html#a70ac32134a2dbf28553f360f40b04d33a9c08498f964d1f9a8b22ccd26da7f3fd',1,'abigail::hashing']]],
  ['hashing_5ffinished_5fstate_9',['HASHING_FINISHED_STATE',['../namespaceabigail_1_1hashing.html#a70ac32134a2dbf28553f360f40b04d33a6e5b82c0f12989b9f975435b7bcb945f',1,'abigail::hashing']]],
  ['hashing_5fnot_5fdone_5fstate_10',['HASHING_NOT_DONE_STATE',['../namespaceabigail_1_1hashing.html#a70ac32134a2dbf28553f360f40b04d33abc373f8f7d53f64b98ecd9390c46b71d',1,'abigail::hashing']]],
  ['hashing_5fstarted_5fstate_11',['HASHING_STARTED_STATE',['../namespaceabigail_1_1hashing.html#a70ac32134a2dbf28553f360f40b04d33a2326011ea6a25a3b97489a5e7ea499bb',1,'abigail::hashing']]],
  ['hashing_5fsubtype_5fstate_12',['HASHING_SUBTYPE_STATE',['../namespaceabigail_1_1hashing.html#a70ac32134a2dbf28553f360f40b04d33aaec73fe77b368d3a34bce6156adc264b',1,'abigail::hashing']]]
];
