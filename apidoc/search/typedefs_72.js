var searchData=
[
  ['read_5fcontext_5fsptr',['read_context_sptr',['../namespaceabigail_1_1dwarf__reader.html#ac8c5d52bf6bd4691c71df8009168538b',1,'abigail::dwarf_reader::read_context_sptr()'],['../namespaceabigail_1_1xml__reader.html#ac8c5d52bf6bd4691c71df8009168538b',1,'abigail::xml_reader::read_context_sptr()']]],
  ['reader_5fsptr',['reader_sptr',['../namespaceabigail_1_1xml.html#aeeeef7bfc21fbf43ec6bd9c8d1980c01',1,'abigail::xml']]],
  ['reference_5fdiff_5fsptr',['reference_diff_sptr',['../namespaceabigail_1_1comparison.html#a5261ada365355ede75c53a5a2591de6e',1,'abigail::comparison']]],
  ['reference_5ftype_5fdef_5fsptr',['reference_type_def_sptr',['../namespaceabigail_1_1ir.html#a040c9bee81d2bce0658a622d198c7799',1,'abigail::ir']]],
  ['regex_5ft_5fsptr',['regex_t_sptr',['../namespaceabigail_1_1regex.html#ad7f118099decbc0d358247c3c710398f',1,'abigail::regex']]],
  ['regex_5ft_5fsptrs_5ftype',['regex_t_sptrs_type',['../namespaceabigail_1_1ir.html#a725fc967c76c0336331dba0764efcd8b',1,'abigail::ir']]],
  ['reporter_5fbase_5fsptr',['reporter_base_sptr',['../namespaceabigail_1_1comparison.html#a106cc8cd03a8c471d578ca39d07f82ba',1,'abigail::comparison']]]
];
