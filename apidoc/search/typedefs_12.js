var searchData=
[
  ['var_5fdecl_5fsptr_0',['var_decl_sptr',['../namespaceabigail_1_1ir.html#a8301b04875cd73c44a1e395d6fcae1f2',1,'abigail::ir']]],
  ['var_5fdecl_5fwptr_1',['var_decl_wptr',['../namespaceabigail_1_1ir.html#a10fd3b6399ebb0678957eef33d0cd820',1,'abigail::ir']]],
  ['var_5fdiff_5fsptr_2',['var_diff_sptr',['../namespaceabigail_1_1comparison.html#a500f75478e4d4f406fc1331757f13933',1,'abigail::comparison']]],
  ['var_5fdiff_5fsptrs_5ftype_3',['var_diff_sptrs_type',['../namespaceabigail_1_1comparison.html#a15d148407b34ca4e3df1013d714286a7',1,'abigail::comparison']]],
  ['var_5fptr_5fmap_5ftype_4',['var_ptr_map_type',['../namespaceabigail_1_1ir.html#a996133871c9281e307571523f30652f9',1,'abigail::ir']]],
  ['variable_5fsuppression_5fsptr_5',['variable_suppression_sptr',['../namespaceabigail_1_1suppr.html#a152f8a9314ee9539308d002a1b867f2c',1,'abigail::suppr']]],
  ['variable_5fsuppressions_5ftype_6',['variable_suppressions_type',['../namespaceabigail_1_1suppr.html#a3dae3b9940672cdb208e0792f871097c',1,'abigail::suppr']]],
  ['variables_7',['variables',['../classabigail_1_1ir_1_1corpus.html#acab3e9ea37039063f0692e20517d58a7',1,'abigail::ir::corpus']]],
  ['variables_5fset_8',['variables_set',['../classabigail_1_1ir_1_1corpus.html#a68de21e718fa4d524e66faad2908ad82',1,'abigail::ir::corpus']]],
  ['virtual_5fmem_5ffn_5fmap_5ftype_9',['virtual_mem_fn_map_type',['../classabigail_1_1ir_1_1class__or__union.html#a1026b03ed02adfb5812ea44cfe7bc68f',1,'abigail::ir::class_or_union']]]
];
