var searchData=
[
  ['reader_5fsptr_0',['reader_sptr',['../namespaceabigail_1_1elf.html#a99185c8e2b8bb4694d8430c6f87ca71b',1,'abigail::elf::reader_sptr'],['../namespaceabigail_1_1xml.html#aeeeef7bfc21fbf43ec6bd9c8d1980c01',1,'abigail::xml::reader_sptr']]],
  ['reference_5fdiff_5fsptr_1',['reference_diff_sptr',['../namespaceabigail_1_1comparison.html#a5261ada365355ede75c53a5a2591de6e',1,'abigail::comparison']]],
  ['reference_5ftype_5fdef_5fsptr_2',['reference_type_def_sptr',['../namespaceabigail_1_1ir.html#a040c9bee81d2bce0658a622d198c7799',1,'abigail::ir']]],
  ['regex_5ft_5fsptr_3',['regex_t_sptr',['../namespaceabigail_1_1regex.html#acb99eb8ec0e64e976ac82349523f9e71',1,'abigail::regex']]],
  ['regex_5ft_5fsptrs_5ftype_4',['regex_t_sptrs_type',['../namespaceabigail_1_1ir.html#a725fc967c76c0336331dba0764efcd8b',1,'abigail::ir']]],
  ['reporter_5fbase_5fsptr_5',['reporter_base_sptr',['../namespaceabigail_1_1comparison.html#a106cc8cd03a8c471d578ca39d07f82ba',1,'abigail::comparison']]]
];
