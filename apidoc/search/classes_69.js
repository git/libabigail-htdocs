var searchData=
[
  ['insertion',['insertion',['../classabigail_1_1diff__utils_1_1insertion.html',1,'abigail::diff_utils']]],
  ['insertion_5frange',['insertion_range',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range.html',1,'abigail::suppr::type_suppression']]],
  ['integer_5fboundary',['integer_boundary',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range_1_1integer__boundary.html',1,'abigail::suppr::type_suppression::insertion_range']]],
  ['integral_5ftype',['integral_type',['../classabigail_1_1ir_1_1integral__type.html',1,'abigail::ir']]],
  ['interned_5fstring',['interned_string',['../classabigail_1_1interned__string.html',1,'abigail']]],
  ['interned_5fstring_5fpool',['interned_string_pool',['../classabigail_1_1interned__string__pool.html',1,'abigail']]],
  ['ir_5fnode_5fvisitor',['ir_node_visitor',['../classabigail_1_1ir_1_1ir__node__visitor.html',1,'abigail::ir']]],
  ['ir_5ftraversable_5fbase',['ir_traversable_base',['../structabigail_1_1ir_1_1ir__traversable__base.html',1,'abigail::ir']]]
];
