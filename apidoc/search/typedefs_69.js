var searchData=
[
  ['imported_5funit_5fpoints_5ftype',['imported_unit_points_type',['../namespaceabigail_1_1dwarf__reader.html#aea188110ff8ce2029c68b4e4bb62bbb3',1,'abigail::dwarf_reader']]],
  ['insertion_5frange_5fsptr',['insertion_range_sptr',['../classabigail_1_1suppr_1_1type__suppression.html#af41f7f980a3047cdfb6c4141eb7e2a1d',1,'abigail::suppr::type_suppression']]],
  ['insertion_5franges',['insertion_ranges',['../classabigail_1_1suppr_1_1type__suppression.html#a3dffddbbbcff12afbefe9c893877ac67',1,'abigail::suppr::type_suppression']]],
  ['integer_5fboundary_5fsptr',['integer_boundary_sptr',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range.html#a75b46cfde6f3d029f140924089197fbd',1,'abigail::suppr::type_suppression::insertion_range']]],
  ['interned_5fstring_5fbool_5fmap_5ftype',['interned_string_bool_map_type',['../namespaceabigail_1_1ir.html#a7fa66b1fb3053a4472d94f679bc44ef0',1,'abigail::ir']]],
  ['interned_5fstring_5fset_5ftype',['interned_string_set_type',['../namespaceabigail.html#aabeb19c5f616e214809851e2773fbea8',1,'abigail']]],
  ['ir_5ftraversable_5fbase_5fsptr',['ir_traversable_base_sptr',['../namespaceabigail_1_1ir.html#aa69b53915d71242a426937dd18ab9cb4',1,'abigail::ir']]],
  ['istring_5fdwarf_5foffsets_5fmap_5ftype',['istring_dwarf_offsets_map_type',['../namespaceabigail_1_1dwarf__reader.html#ae0b1a7ff24d3b77a509ca4fc3044a608',1,'abigail::dwarf_reader']]],
  ['istring_5ffn_5ftype_5fmap_5ftype',['istring_fn_type_map_type',['../namespaceabigail_1_1dwarf__reader.html#a71cabbdfa1a6115f761164afcee54372',1,'abigail::dwarf_reader']]],
  ['istring_5ftype_5fbase_5fwptr_5fmap_5ftype',['istring_type_base_wptr_map_type',['../namespaceabigail_1_1ir.html#abb04208e104558ecef9652212a664b78',1,'abigail::ir']]],
  ['istring_5ftype_5fbase_5fwptrs_5fmap_5ftype',['istring_type_base_wptrs_map_type',['../namespaceabigail_1_1ir.html#a81a7885148c2cac4f666daeb53e30e44',1,'abigail::ir']]],
  ['istring_5ftype_5for_5fdecl_5fbase_5fsptr_5fmap_5ftype',['istring_type_or_decl_base_sptr_map_type',['../namespaceabigail_1_1ir.html#adb9d8b24cd5ed1ad8f1b4e699630a567',1,'abigail::ir']]]
];
