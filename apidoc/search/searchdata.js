var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwx~",
  1: "abcdefghilmnopqrstuv",
  2: "a",
  3: "at",
  4: "abcdefghiklmnopqrstuvwx~",
  5: "aelp",
  6: "abcdefghilmnoprstuvwx",
  7: "abcdefhklmorstuv",
  8: "abcdefhilnprstuvw",
  9: "acefgikmoprsv",
  10: "acrsx",
  11: "cdeimnortw",
  12: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

