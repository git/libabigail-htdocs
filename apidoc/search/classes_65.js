var searchData=
[
  ['edit_5fscript',['edit_script',['../classabigail_1_1diff__utils_1_1edit__script.html',1,'abigail::diff_utils']]],
  ['elf_5fsymbol',['elf_symbol',['../classabigail_1_1ir_1_1elf__symbol.html',1,'abigail::ir']]],
  ['elf_5fsymbol_5fcomp',['elf_symbol_comp',['../structabigail_1_1comparison_1_1elf__symbol__comp.html',1,'abigail::comparison']]],
  ['enum_5fdiff',['enum_diff',['../classabigail_1_1comparison_1_1enum__diff.html',1,'abigail::comparison']]],
  ['enum_5ftype_5fdecl',['enum_type_decl',['../classabigail_1_1ir_1_1enum__type__decl.html',1,'abigail::ir']]],
  ['enumerator',['enumerator',['../classabigail_1_1ir_1_1enum__type__decl_1_1enumerator.html',1,'abigail::ir::enum_type_decl']]],
  ['enumerator_5fvalue_5fcomp',['enumerator_value_comp',['../structabigail_1_1comparison_1_1enumerator__value__comp.html',1,'abigail::comparison']]],
  ['environment',['environment',['../classabigail_1_1ir_1_1environment.html',1,'abigail::ir']]],
  ['escape',['escape',['../structabigail_1_1regex_1_1escape.html',1,'abigail::regex']]],
  ['exported_5fdecls_5fbuilder',['exported_decls_builder',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder.html',1,'abigail::ir::corpus']]]
];
