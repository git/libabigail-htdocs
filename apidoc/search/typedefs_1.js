var searchData=
[
  ['base_5fdiff_5fsptr_0',['base_diff_sptr',['../namespaceabigail_1_1comparison.html#a99d605a5604417f3d16bdb175878a24c',1,'abigail::comparison']]],
  ['base_5fdiff_5fsptrs_5ftype_1',['base_diff_sptrs_type',['../namespaceabigail_1_1comparison.html#a54e38b18ca6427f12398a608e8803f62',1,'abigail::comparison']]],
  ['base_5fiterator_2',['base_iterator',['../abg-symtab-reader_8h.html#a83694f7058e680c7fd3e325e3ea817dd',1,'abigail::symtab_reader']]],
  ['base_5fspec_5fsptr_3',['base_spec_sptr',['../classabigail_1_1ir_1_1class__decl.html#a06b70efb702f550ff5d68820d3be8635',1,'abigail::ir::class_decl']]],
  ['base_5fspecs_4',['base_specs',['../classabigail_1_1ir_1_1class__decl.html#a9d63c3dc14414d0c9e47fb483efce6ee',1,'abigail::ir::class_decl']]],
  ['boundary_5fsptr_5',['boundary_sptr',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range.html#af2a1d9faac8591951f069ac821e2c616',1,'abigail::suppr::type_suppression::insertion_range']]]
];
