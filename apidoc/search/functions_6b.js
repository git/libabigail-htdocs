var searchData=
[
  ['keep_5fdiff_5falive',['keep_diff_alive',['../classabigail_1_1comparison_1_1diff__context.html#a2c5a26a9a9383dc77f2758dc3082e3a1',1,'abigail::comparison::diff_context']]],
  ['keep_5ftype_5falive',['keep_type_alive',['../namespaceabigail_1_1ir.html#aaa933feb1ee738ca0f64309d7d276e80',1,'abigail::ir']]],
  ['keep_5fwrt_5fid_5fof_5ffns_5fto_5fkeep',['keep_wrt_id_of_fns_to_keep',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#a0fad1fb3c7cac8154bcaed420c664811',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['keep_5fwrt_5fid_5fof_5fvars_5fto_5fkeep',['keep_wrt_id_of_vars_to_keep',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#a2ba75e263d322fd360dc2f7dedc6773b',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['keep_5fwrt_5fregex_5fof_5ffns_5fto_5fkeep',['keep_wrt_regex_of_fns_to_keep',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#a9197b596a66a0fbed08d78fa08c9a769',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['keep_5fwrt_5fregex_5fof_5ffns_5fto_5fsuppress',['keep_wrt_regex_of_fns_to_suppress',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#ac8f2cfe9155966f1b120a0ceb3e636fa',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['keep_5fwrt_5fregex_5fof_5fvars_5fto_5fkeep',['keep_wrt_regex_of_vars_to_keep',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#a6512f3a577933386a40a806d41e6e1a9',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['keep_5fwrt_5fregex_5fof_5fvars_5fto_5fsuppress',['keep_wrt_regex_of_vars_to_suppress',['../classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html#abfc4879e16bc345ec5e163d4143772f2',1,'abigail::ir::corpus::exported_decls_builder::priv']]],
  ['kind',['kind',['../classabigail_1_1ir_1_1type__or__decl__base.html#a8b589799568aa3604cbb87df2a3758fe',1,'abigail::ir::type_or_decl_base::kind() const '],['../classabigail_1_1ir_1_1type__or__decl__base.html#a7d26d50655b22c9c76c0d73b50341b6b',1,'abigail::ir::type_or_decl_base::kind(enum type_or_decl_kind)']]]
];
