var searchData=
[
  ['elf_5fsymbol_5fsptr',['elf_symbol_sptr',['../namespaceabigail_1_1ir.html#a313302c342a903ff16cb46d8e457fef4',1,'abigail::ir']]],
  ['elf_5fsymbol_5fwptr',['elf_symbol_wptr',['../namespaceabigail_1_1ir.html#a9d1ea035b8e509086e02dbc26feb349e',1,'abigail::ir']]],
  ['elf_5fsymbols',['elf_symbols',['../namespaceabigail_1_1ir.html#a4ec7ed8654859e7f0888640b6a893c6a',1,'abigail::ir']]],
  ['enum_5ftype_5fdecl_5fsptr',['enum_type_decl_sptr',['../namespaceabigail_1_1ir.html#ab069e4c80c8cb856a85644bbeeb1fc5b',1,'abigail::ir']]],
  ['enumerators',['enumerators',['../classabigail_1_1ir_1_1enum__type__decl.html#a4dedb8393999c52a4265aeb4639e665f',1,'abigail::ir::enum_type_decl']]],
  ['enums_5ftype',['enums_type',['../namespaceabigail_1_1ir.html#af32af7b69d2d1d0c252036c3ddf65075',1,'abigail::ir']]],
  ['environment_5fsptr',['environment_sptr',['../namespaceabigail_1_1ir.html#a8b1f2783fa94954ff476bbde9bf9833a',1,'abigail::ir']]],
  ['exported_5fdecls_5fbuilder_5fsptr',['exported_decls_builder_sptr',['../classabigail_1_1ir_1_1corpus.html#a342d2365a9c44a2c4878cb58d0092b55',1,'abigail::ir::corpus']]]
];
