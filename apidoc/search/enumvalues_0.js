var searchData=
[
  ['abidiff_5fabi_5fchange_0',['ABIDIFF_ABI_CHANGE',['../namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737a6c13b120f87d82c6cc50bc8728c1cfb7',1,'abigail::tools_utils']]],
  ['abidiff_5fabi_5fincompatible_5fchange_1',['ABIDIFF_ABI_INCOMPATIBLE_CHANGE',['../namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737a42834064aede03fc472460c43da7944b',1,'abigail::tools_utils']]],
  ['abidiff_5ferror_2',['ABIDIFF_ERROR',['../namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737ab888207fb85b2094b967aa0e3224a6cb',1,'abigail::tools_utils']]],
  ['abidiff_5fok_3',['ABIDIFF_OK',['../namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737acb017fb6269a071396bd2122a3600e4e',1,'abigail::tools_utils']]],
  ['abidiff_5fusage_5ferror_4',['ABIDIFF_USAGE_ERROR',['../namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737ab58d4752cfa3cc08cafc4487c9566510',1,'abigail::tools_utils']]],
  ['access_5fchange_5fcategory_5',['ACCESS_CHANGE_CATEGORY',['../namespaceabigail_1_1comparison.html#a0f2338d52029e18be6fb9af206ecc2b9a199e86e8f39a08766a9de3ebcccc1c00',1,'abigail::comparison']]],
  ['added_5ffunction_5fchange_5fkind_6',['ADDED_FUNCTION_CHANGE_KIND',['../classabigail_1_1suppr_1_1function__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a391373998159aaf7ad48b365f749590c',1,'abigail::suppr::function_suppression']]],
  ['added_5fvariable_5fchange_5fkind_7',['ADDED_VARIABLE_CHANGE_KIND',['../classabigail_1_1suppr_1_1variable__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a19c7d3bf940d24b404ce11721043c58b',1,'abigail::suppr::variable_suppression']]],
  ['all_5fchange_5fkind_8',['ALL_CHANGE_KIND',['../classabigail_1_1suppr_1_1function__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a24fbb4e050ab209abbb464bd9d71bdfa',1,'abigail::suppr::function_suppression::ALL_CHANGE_KIND'],['../classabigail_1_1suppr_1_1variable__suppression.html#a38c0aa37ddd59d4ab5916634c50967a7a24fbb4e050ab209abbb464bd9d71bdfa',1,'abigail::suppr::variable_suppression::ALL_CHANGE_KIND']]],
  ['all_5flocal_5fchanges_5fmask_9',['ALL_LOCAL_CHANGES_MASK',['../namespaceabigail_1_1ir.html#a38c0aa37ddd59d4ab5916634c50967a7a82d8a46739830b986b89950d25a67de5',1,'abigail::ir']]],
  ['array_5fsize_5fbase_5ftype_10',['ARRAY_SIZE_BASE_TYPE',['../classabigail_1_1ir_1_1real__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9ae4a32fdc8e45938999aa7f58b0d36d40',1,'abigail::ir::real_type']]]
];
