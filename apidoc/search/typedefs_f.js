var searchData=
[
  ['scope_5fdecl_5fsptr_0',['scope_decl_sptr',['../namespaceabigail_1_1ir.html#a87be4b552f87f51d4431f1386c90224c',1,'abigail::ir']]],
  ['scope_5fdiff_5fsptr_1',['scope_diff_sptr',['../namespaceabigail_1_1comparison.html#a9a08e4ee184c95969ea792e19e084f4d',1,'abigail::comparison']]],
  ['scope_5fstack_5ftype_2',['scope_stack_type',['../namespaceabigail_1_1dwarf.html#ac185c71da0cab48e4f153a66bb7d7ed6',1,'abigail::dwarf']]],
  ['scopes_3',['scopes',['../classabigail_1_1ir_1_1scope__decl.html#a0d7c3cbf2fc3cfb2e706284488146894',1,'abigail::ir::scope_decl']]],
  ['section_5fsptr_4',['section_sptr',['../classabigail_1_1ini_1_1config.html#ae730273a63cc1dda3fe4db5bf97a9c11',1,'abigail::ini::config']]],
  ['sections_5ftype_5',['sections_type',['../classabigail_1_1ini_1_1config.html#a48ed5126e0f66786cb8ffb0ceec749a8',1,'abigail::ini::config']]],
  ['simple_5fproperty_5fsptr_6',['simple_property_sptr',['../namespaceabigail_1_1ini.html#a8d591727ae180ad9a8e183a982b4358b',1,'abigail::ini']]],
  ['str_5ffn_5fptr_5fmap_5ftype_7',['str_fn_ptr_map_type',['../namespaceabigail_1_1ir.html#a60876b18a5490a76007acf258633049e',1,'abigail::ir']]],
  ['str_5ffn_5fptr_5fset_5fmap_5ftype_8',['str_fn_ptr_set_map_type',['../namespaceabigail_1_1ir.html#aac1c0341138e0059a9010c250c4dd0d2',1,'abigail::ir']]],
  ['str_5ffn_5fptrs_5fmap_5ftype_9',['str_fn_ptrs_map_type',['../namespaceabigail_1_1ir.html#aeea8897eca35b1567285f774f3fc5af2',1,'abigail::ir']]],
  ['str_5fvar_5fptr_5fmap_5ftype_10',['str_var_ptr_map_type',['../namespaceabigail_1_1ir.html#aa68c9370650a7b37d9d198bd1a8f5efa',1,'abigail::ir']]],
  ['string_5fbase_5fdiff_5fsptr_5fmap_11',['string_base_diff_sptr_map',['../namespaceabigail_1_1comparison.html#aba0a3c82b94cf21fffb492eb164e3b83',1,'abigail::comparison']]],
  ['string_5fbase_5fsptr_5fmap_12',['string_base_sptr_map',['../namespaceabigail_1_1comparison.html#a1876c2cced5b479fc0344953b84af145',1,'abigail::comparison']]],
  ['string_5fchanged_5fenumerator_5fmap_13',['string_changed_enumerator_map',['../namespaceabigail_1_1comparison.html#a8b96530273ccae29195d44452fae21b0',1,'abigail::comparison']]],
  ['string_5fchanged_5fmember_5ffunction_5fsptr_5fmap_14',['string_changed_member_function_sptr_map',['../namespaceabigail_1_1comparison.html#a3c659b4f8ef8640713eb242f1520cdf5',1,'abigail::comparison']]],
  ['string_5fclasses_5fmap_15',['string_classes_map',['../namespaceabigail_1_1dwarf.html#a1e24f12bc00af3cab3d8fcf002b1a744',1,'abigail::dwarf']]],
  ['string_5fclasses_5for_5funions_5fmap_16',['string_classes_or_unions_map',['../namespaceabigail_1_1dwarf.html#a897c0b693c7f1d4473797b5ba9d558c9',1,'abigail::dwarf']]],
  ['string_5fdecl_5fbase_5fsptr_5fmap_17',['string_decl_base_sptr_map',['../namespaceabigail_1_1ir.html#a93f0ad8676eb61e0a42b77969a2c57d9',1,'abigail::ir']]],
  ['string_5fdecl_5fdiff_5fbase_5fsptr_5fmap_18',['string_decl_diff_base_sptr_map',['../namespaceabigail_1_1comparison.html#ae375ba21e94d2b6cdf00083c5dbb07d7',1,'abigail::comparison']]],
  ['string_5fdiff_5fptr_5fmap_19',['string_diff_ptr_map',['../namespaceabigail_1_1comparison.html#ad37207696cdfc0d54f8d4abaea946b90',1,'abigail::comparison']]],
  ['string_5fdiff_5fsptr_5fmap_20',['string_diff_sptr_map',['../namespaceabigail_1_1comparison.html#acfcebd7d7dd28d225d919a4192bb3802',1,'abigail::comparison']]],
  ['string_5felf_5fsymbol_5fmap_21',['string_elf_symbol_map',['../namespaceabigail_1_1comparison.html#ae6b6fee84dd6dac4fcead41ea2e219a4',1,'abigail::comparison']]],
  ['string_5felf_5fsymbol_5fsptr_5fmap_5fsptr_22',['string_elf_symbol_sptr_map_sptr',['../namespaceabigail_1_1ir.html#a93cf21703174bb7d5d4fd3a83d450ecb',1,'abigail::ir']]],
  ['string_5felf_5fsymbol_5fsptr_5fmap_5ftype_23',['string_elf_symbol_sptr_map_type',['../namespaceabigail_1_1ir.html#a7b7644d5928ac0c1a537c6c2a647e8dd',1,'abigail::ir']]],
  ['string_5felf_5fsymbols_5fmap_5fsptr_24',['string_elf_symbols_map_sptr',['../namespaceabigail_1_1ir.html#a8c1b88b8b88003c46fa14ed82f8b11d5',1,'abigail::ir']]],
  ['string_5felf_5fsymbols_5fmap_5ftype_25',['string_elf_symbols_map_type',['../namespaceabigail_1_1ir.html#ae957523577cd705b73c2f8a2b31c96a3',1,'abigail::ir']]],
  ['string_5fenumerator_5fmap_26',['string_enumerator_map',['../namespaceabigail_1_1comparison.html#a4dd2fbb6584475756f35bb677663fd5d',1,'abigail::comparison']]],
  ['string_5fenums_5fmap_27',['string_enums_map',['../namespaceabigail_1_1dwarf.html#abcb5c2d2aa30a78282ae2ce8634fbd29',1,'abigail::dwarf']]],
  ['string_5ffn_5fparm_5fdiff_5fsptr_5fmap_28',['string_fn_parm_diff_sptr_map',['../namespaceabigail_1_1comparison.html#a8ee49ebad8470f70ad374a1f458362cf',1,'abigail::comparison']]],
  ['string_5ffunction_5fdecl_5fdiff_5fsptr_5fmap_29',['string_function_decl_diff_sptr_map',['../namespaceabigail_1_1comparison.html#a67dc2ce9945cd6da083a2a07c60e9e70',1,'abigail::comparison']]],
  ['string_5ffunction_5fptr_5fmap_30',['string_function_ptr_map',['../namespaceabigail_1_1comparison.html#af9c98b3217cf861d49988c87f18f5a96',1,'abigail::comparison']]],
  ['string_5fmem_5ffn_5fptr_5fmap_5ftype_31',['string_mem_fn_ptr_map_type',['../classabigail_1_1ir_1_1class__or__union.html#afdaa434637b27b616103193b2a008cb7',1,'abigail::ir::class_or_union']]],
  ['string_5fmem_5ffn_5fsptr_5fmap_5ftype_32',['string_mem_fn_sptr_map_type',['../classabigail_1_1ir_1_1class__or__union.html#a264755aec245ce7daed3572c2f45fe77',1,'abigail::ir::class_or_union']]],
  ['string_5fmember_5ffunction_5fsptr_5fmap_33',['string_member_function_sptr_map',['../namespaceabigail_1_1comparison.html#a50557f8615dd7f24d01421614e3d67ed',1,'abigail::comparison']]],
  ['string_5fparm_5fmap_34',['string_parm_map',['../namespaceabigail_1_1comparison.html#a4b10f5e01da4fb5d6663864437d80e7b',1,'abigail::comparison']]],
  ['string_5fproperty_5fvalue_5fsptr_35',['string_property_value_sptr',['../namespaceabigail_1_1ini.html#a9c266dd2899f675bd5122dbda18e6bf3',1,'abigail::ini']]],
  ['string_5fstrings_5fmap_5ftype_36',['string_strings_map_type',['../namespaceabigail_1_1abixml.html#a1ef30bd82bab935f367ff4f609794dab',1,'abigail::abixml']]],
  ['string_5ftu_5fmap_5ftype_37',['string_tu_map_type',['../namespaceabigail_1_1ir.html#aa12800bd9ae5bf6deaf71d09e0e51084',1,'abigail::ir']]],
  ['string_5ftype_5fbase_5fsptr_5fmap_38',['string_type_base_sptr_map',['../namespaceabigail_1_1comparison.html#a38dcd1879e62157d730db55ec8021c46',1,'abigail::comparison']]],
  ['string_5ftype_5fbase_5fsptr_5fmap_5ftype_39',['string_type_base_sptr_map_type',['../namespaceabigail_1_1ir.html#a0b64a78d29c377617cf90b8dae91b6d4',1,'abigail::ir']]],
  ['string_5ftype_5fbase_5fwptr_5fmap_5ftype_40',['string_type_base_wptr_map_type',['../namespaceabigail_1_1ir.html#af1f1cce370d89251dfc8d5b1b3d31bdb',1,'abigail::ir']]],
  ['string_5ftype_5fdiff_5fbase_5fsptr_5fmap_41',['string_type_diff_base_sptr_map',['../namespaceabigail_1_1comparison.html#aa6535fcacbdf24048b5feb09250052fc',1,'abigail::comparison']]],
  ['string_5fvar_5fdiff_5fptr_5fmap_42',['string_var_diff_ptr_map',['../namespaceabigail_1_1comparison.html#a1f3bfb2d79eda0502c58719e04e42ffa',1,'abigail::comparison']]],
  ['string_5fvar_5fdiff_5fsptr_5fmap_43',['string_var_diff_sptr_map',['../namespaceabigail_1_1comparison.html#a10d74e8957acea1ea5be9fecfd2ca9f7',1,'abigail::comparison']]],
  ['string_5fvar_5fptr_5fmap_44',['string_var_ptr_map',['../namespaceabigail_1_1comparison.html#a8f50c05e62eb5268466185aad071916b',1,'abigail::comparison']]],
  ['strings_5ftype_45',['strings_type',['../classabigail_1_1ir_1_1corpus.html#ac81176f065a7522b25c1a7620704de59',1,'abigail::ir::corpus']]],
  ['subrange_5fdiff_5fsptr_46',['subrange_diff_sptr',['../namespaceabigail_1_1comparison.html#a49b249c66548e7309001a860779bf082',1,'abigail::comparison']]],
  ['subrange_5fsptr_47',['subrange_sptr',['../classabigail_1_1ir_1_1array__type__def.html#ab333b50d7ad53c6d8ed4a864d4b80c87',1,'abigail::ir::array_type_def']]],
  ['subranges_5ftype_48',['subranges_type',['../classabigail_1_1ir_1_1array__type__def.html#a5042f9f792517f95a4ecd35797ed8ecc',1,'abigail::ir::array_type_def']]],
  ['suppression_5fsptr_49',['suppression_sptr',['../namespaceabigail_1_1suppr.html#ae206b0975862f7d6c992d6e888225842',1,'abigail::suppr']]],
  ['suppressions_5ftype_50',['suppressions_type',['../namespaceabigail_1_1suppr.html#a1b074266b52cf03703a6ff031350d678',1,'abigail::suppr']]],
  ['symtab_5fsptr_51',['symtab_sptr',['../abg-fwd_8h.html#acdbd626265b1c85032b417b012bb492a',1,'abigail::symtab_reader']]]
];
