var searchData=
[
  ['named_5fboundary_5fsptr_0',['named_boundary_sptr',['../classabigail_1_1suppr_1_1type__suppression_1_1insertion__range.html#af9a08bbda0b864aea1f1d1e39778befa',1,'abigail::suppr::type_suppression::insertion_range']]],
  ['namespace_5fdecl_5fsptr_1',['namespace_decl_sptr',['../namespaceabigail_1_1ir.html#ad3807c6d4505783bbcf75e5a928f0317',1,'abigail::ir']]],
  ['namespaces_5ftype_2',['namespaces_type',['../namespaceabigail_1_1ir.html#abca41df3dcd9b28d1f00a0c4dd2bfcbc',1,'abigail::ir']]],
  ['nc_5ftype_5fptr_5fistr_5fmap_5ftype_3',['nc_type_ptr_istr_map_type',['../namespaceabigail_1_1xml__writer.html#ae34b701b3a355c3d8e857e24c040f07a',1,'abigail::xml_writer']]],
  ['nc_5ftype_5fptr_5fset_5ftype_4',['nc_type_ptr_set_type',['../namespaceabigail_1_1xml__writer.html#af18dbe703324f47d279362619c90eea6',1,'abigail::xml_writer']]],
  ['negated_5fsuppression_5fsptr_5',['negated_suppression_sptr',['../namespaceabigail_1_1suppr.html#a170c82cabd4a2cb13321443b9bf4f667',1,'abigail::suppr']]],
  ['negated_5fsuppressions_5ftype_6',['negated_suppressions_type',['../namespaceabigail_1_1suppr.html#a1197ae764a7e00e0b0de72eb3e320014',1,'abigail::suppr']]],
  ['non_5ftype_5ftparameter_5fsptr_7',['non_type_tparameter_sptr',['../namespaceabigail_1_1ir.html#a34264a4a00fbd62333de6b204942210d',1,'abigail::ir']]]
];
