var searchData=
[
  ['d_5fpath_5fvec_0',['d_path_vec',['../classabigail_1_1diff__utils_1_1d__path__vec.html',1,'abigail::diff_utils']]],
  ['data_5fmember_5fcomp_1',['data_member_comp',['../structabigail_1_1comparison_1_1data__member__comp.html',1,'abigail::comparison']]],
  ['data_5fmember_5fdiff_5fcomp_2',['data_member_diff_comp',['../structabigail_1_1comparison_1_1data__member__diff__comp.html',1,'abigail::comparison']]],
  ['decl_5fbase_3',['decl_base',['../classabigail_1_1ir_1_1decl__base.html',1,'abigail::ir']]],
  ['decl_5fdiff_5fbase_4',['decl_diff_base',['../classabigail_1_1comparison_1_1decl__diff__base.html',1,'abigail::comparison']]],
  ['decl_5ftopo_5fcomp_5',['decl_topo_comp',['../structabigail_1_1ir_1_1decl__topo__comp.html',1,'abigail::ir']]],
  ['deep_5fptr_5feq_5ffunctor_6',['deep_ptr_eq_functor',['../structabigail_1_1diff__utils_1_1deep__ptr__eq__functor.html',1,'abigail::diff_utils']]],
  ['default_5feq_5ffunctor_7',['default_eq_functor',['../structabigail_1_1diff__utils_1_1default__eq__functor.html',1,'abigail::diff_utils']]],
  ['default_5freporter_8',['default_reporter',['../classabigail_1_1comparison_1_1default__reporter.html',1,'abigail::comparison']]],
  ['deletion_9',['deletion',['../classabigail_1_1diff__utils_1_1deletion.html',1,'abigail::diff_utils']]],
  ['diff_10',['diff',['../classabigail_1_1comparison_1_1diff.html',1,'abigail::comparison']]],
  ['diff_5fcomp_11',['diff_comp',['../structabigail_1_1comparison_1_1diff__comp.html',1,'abigail::comparison']]],
  ['diff_5fcontext_12',['diff_context',['../classabigail_1_1comparison_1_1diff__context.html',1,'abigail::comparison']]],
  ['diff_5fequal_13',['diff_equal',['../structabigail_1_1comparison_1_1diff__equal.html',1,'abigail::comparison']]],
  ['diff_5fhash_14',['diff_hash',['../structabigail_1_1comparison_1_1diff__hash.html',1,'abigail::comparison']]],
  ['diff_5fless_5fthan_5ffunctor_15',['diff_less_than_functor',['../structabigail_1_1comparison_1_1diff__less__than__functor.html',1,'abigail::comparison']]],
  ['diff_5fmaps_16',['diff_maps',['../classabigail_1_1comparison_1_1diff__maps.html',1,'abigail::comparison']]],
  ['diff_5fnode_5fvisitor_17',['diff_node_visitor',['../classabigail_1_1comparison_1_1diff__node__visitor.html',1,'abigail::comparison']]],
  ['diff_5fsptr_5fhasher_18',['diff_sptr_hasher',['../structabigail_1_1comparison_1_1diff__sptr__hasher.html',1,'abigail::comparison']]],
  ['diff_5fstats_19',['diff_stats',['../classabigail_1_1comparison_1_1corpus__diff_1_1diff__stats.html',1,'abigail::comparison::corpus_diff']]],
  ['diff_5ftraversable_5fbase_20',['diff_traversable_base',['../classabigail_1_1comparison_1_1diff__traversable__base.html',1,'abigail::comparison']]],
  ['distinct_5fdiff_21',['distinct_diff',['../classabigail_1_1comparison_1_1distinct__diff.html',1,'abigail::comparison']]],
  ['dm_5fcontext_5frel_22',['dm_context_rel',['../classabigail_1_1ir_1_1dm__context__rel.html',1,'abigail::ir']]],
  ['dot_23',['dot',['../structabigail_1_1dot.html',1,'abigail']]],
  ['dwfl_5fdeleter_24',['dwfl_deleter',['../structabigail_1_1elf__helpers_1_1dwfl__deleter.html',1,'abigail::elf_helpers']]]
];
