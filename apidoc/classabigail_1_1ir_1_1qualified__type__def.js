var classabigail_1_1ir_1_1qualified__type__def =
[
    [ "CV", "classabigail_1_1ir_1_1qualified__type__def.html#a68bc96a16293c8bfb3b4dfb600baeba1", [
      [ "CV_NONE", "classabigail_1_1ir_1_1qualified__type__def.html#a68bc96a16293c8bfb3b4dfb600baeba1a6abf4a68c04678c2c7a62ec5e514fa0b", null ],
      [ "CV_CONST", "classabigail_1_1ir_1_1qualified__type__def.html#a68bc96a16293c8bfb3b4dfb600baeba1af4ca5e8736336cc6252f89933e169703", null ],
      [ "CV_VOLATILE", "classabigail_1_1ir_1_1qualified__type__def.html#a68bc96a16293c8bfb3b4dfb600baeba1a3ede124aa73ff32afe72eaf1523d61dc", null ],
      [ "CV_RESTRICT", "classabigail_1_1ir_1_1qualified__type__def.html#a68bc96a16293c8bfb3b4dfb600baeba1ac02caf5e3cd2d56ca8f82ee427182c50", null ]
    ] ],
    [ "qualified_type_def", "classabigail_1_1ir_1_1qualified__type__def.html#aa13900134d10e76b1158942937487306", null ],
    [ "qualified_type_def", "classabigail_1_1ir_1_1qualified__type__def.html#a89d3aa09a8a7c1504912f9f83f6157d8", null ],
    [ "build_name", "classabigail_1_1ir_1_1qualified__type__def.html#ae2fea1347c78947914e9ad3b3cece81e", null ],
    [ "get_cv_quals", "classabigail_1_1ir_1_1qualified__type__def.html#aaa912517d0e02e085b23ef6d77ee9a76", null ],
    [ "get_cv_quals_string_prefix", "classabigail_1_1ir_1_1qualified__type__def.html#a61ca3fd3d5c95be9c9060207802446f3", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1qualified__type__def.html#a48d1909c881954a9ffe4efd640625196", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1qualified__type__def.html#a29015ca799ed6bc508517123347e0958", null ],
    [ "get_size_in_bits", "classabigail_1_1ir_1_1qualified__type__def.html#a34f8b53f2d7ed66ad1a13cf57507d8a0", null ],
    [ "get_underlying_type", "classabigail_1_1ir_1_1qualified__type__def.html#ab21b00c256823331fdbc78c9913ebdce", null ],
    [ "on_canonical_type_set", "classabigail_1_1ir_1_1qualified__type__def.html#a7d86f4ec85b4541fac19c57df5370277", null ],
    [ "operator==", "classabigail_1_1ir_1_1qualified__type__def.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1qualified__type__def.html#a53b7e286d4b7074b7bc33161ba6e1295", null ],
    [ "operator==", "classabigail_1_1ir_1_1qualified__type__def.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "set_cv_quals", "classabigail_1_1ir_1_1qualified__type__def.html#a80e4b09bbd7bac5acc7f057a65ec2266", null ],
    [ "set_underlying_type", "classabigail_1_1ir_1_1qualified__type__def.html#a3407fb92a58b64b9799ff6197d765e0c", null ],
    [ "traverse", "classabigail_1_1ir_1_1qualified__type__def.html#aa0435efbde239af376b5d0dfa16135d2", null ]
];