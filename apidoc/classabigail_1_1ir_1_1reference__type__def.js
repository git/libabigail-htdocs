var classabigail_1_1ir_1_1reference__type__def =
[
    [ "reference_type_def", "classabigail_1_1ir_1_1reference__type__def.html#ad21bf419680ee6feb3940c7dc5b786ca", null ],
    [ "reference_type_def", "classabigail_1_1ir_1_1reference__type__def.html#a686d6c8722cc89f7e3d09f8391c1b494", null ],
    [ "get_pretty_representation", "classabigail_1_1ir_1_1reference__type__def.html#aea2770952dab500dd4565fb7881411f3", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1reference__type__def.html#a48d1909c881954a9ffe4efd640625196", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1reference__type__def.html#a29015ca799ed6bc508517123347e0958", null ],
    [ "on_canonical_type_set", "classabigail_1_1ir_1_1reference__type__def.html#a7d86f4ec85b4541fac19c57df5370277", null ],
    [ "operator==", "classabigail_1_1ir_1_1reference__type__def.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1reference__type__def.html#a2edd0d3d0b0a3f6d6da24f34e4948e3e", null ],
    [ "operator==", "classabigail_1_1ir_1_1reference__type__def.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "set_pointed_to_type", "classabigail_1_1ir_1_1reference__type__def.html#abf065dc7d161cd84d81be9b344d71af2", null ],
    [ "traverse", "classabigail_1_1ir_1_1reference__type__def.html#aa0435efbde239af376b5d0dfa16135d2", null ]
];