var abg_viz_common_8h =
[
    [ "canvas", "structabigail_1_1canvas.html", null ],
    [ "style", "structabigail_1_1style.html", null ],
    [ "typography", "structabigail_1_1typography.html", null ],
    [ "color", "abg-viz-common_8h.html#a37dbdc30935031c05304482e1be89d8f", [
      [ "white", "abg-viz-common_8h.html#a37dbdc30935031c05304482e1be89d8fad508fe45cecaf653904a0e774084bb5c", null ],
      [ "gray25", "abg-viz-common_8h.html#a37dbdc30935031c05304482e1be89d8faca5e44f75072c3457603ab53ae6cb24b", null ],
      [ "gray75", "abg-viz-common_8h.html#a37dbdc30935031c05304482e1be89d8fa116401922cb0c1d9f8c369cafe267715", null ],
      [ "black", "abg-viz-common_8h.html#a37dbdc30935031c05304482e1be89d8fa1ffd9e753c8054cc61456ac7fac1ac89", null ]
    ] ],
    [ "units", "abg-viz-common_8h.html#a2ff02817760e56a95b3b1bc7cfa1b77b", [
      [ "millimeter", "abg-viz-common_8h.html#a2ff02817760e56a95b3b1bc7cfa1b77ba1f19b37530dfd9dbdecb42e0a19bc8d3", null ],
      [ "pixel", "abg-viz-common_8h.html#a2ff02817760e56a95b3b1bc7cfa1b77baab4086ecd47c568d5ba5739d4078988f", null ]
    ] ],
    [ "string_replace", "abg-viz-common_8h.html#a8ebfa3e43b092cd6dfa3cf4cc0f5a256", null ]
];