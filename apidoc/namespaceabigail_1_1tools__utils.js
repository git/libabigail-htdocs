var namespaceabigail_1_1tools__utils =
[
    [ "temp_file", "classabigail_1_1tools__utils_1_1temp__file.html", "classabigail_1_1tools__utils_1_1temp__file" ],
    [ "timer", "classabigail_1_1tools__utils_1_1timer.html", "classabigail_1_1tools__utils_1_1timer" ],
    [ "temp_file_sptr", "namespaceabigail_1_1tools__utils.html#a1758c595091f6eb5d530280790bc7772", null ],
    [ "abidiff_status", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737", [
      [ "ABIDIFF_OK", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737acb017fb6269a071396bd2122a3600e4e", null ],
      [ "ABIDIFF_ERROR", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737ab888207fb85b2094b967aa0e3224a6cb", null ],
      [ "ABIDIFF_USAGE_ERROR", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737ab58d4752cfa3cc08cafc4487c9566510", null ],
      [ "ABIDIFF_ABI_CHANGE", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737a6c13b120f87d82c6cc50bc8728c1cfb7", null ],
      [ "ABIDIFF_ABI_INCOMPATIBLE_CHANGE", "namespaceabigail_1_1tools__utils.html#a98bcd83bc6200edd9d44949a820a2737a42834064aede03fc472460c43da7944b", null ]
    ] ],
    [ "file_type", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128", [
      [ "FILE_TYPE_UNKNOWN", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a5a782ca7a5216fdcfb9ff6fb4666525f", null ],
      [ "FILE_TYPE_NATIVE_BI", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a0e578dbff0b68524c58f393a4d4317a1", null ],
      [ "FILE_TYPE_ELF", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a4c9e7ee116baf10f0cf0044d017bf10b", null ],
      [ "FILE_TYPE_AR", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128aed8b6b3fb7c0f64b4060f103f2f30dc0", null ],
      [ "FILE_TYPE_XML_CORPUS", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a299a03a52346a22adcc60330e8eee7ec", null ],
      [ "FILE_TYPE_XML_CORPUS_GROUP", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a39ea99690dbf7ab192bed1174eb96f98", null ],
      [ "FILE_TYPE_RPM", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a00be988028fef8b4cf9297c4a7c0da66", null ],
      [ "FILE_TYPE_SRPM", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128af523c0edb6d25761799aaf0117862aa9", null ],
      [ "FILE_TYPE_DEB", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128a50f10947ad2e607098d31f959365e825", null ],
      [ "FILE_TYPE_DIR", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128aebcae9d844ae4c0785cb14f17c8481ec", null ],
      [ "FILE_TYPE_TAR", "namespaceabigail_1_1tools__utils.html#aaf047b6ce458f7c55fd215920656d128ab9f94dd3ca67d007b5a15a864affaaa0", null ]
    ] ],
    [ "abidiff_status_has_abi_change", "namespaceabigail_1_1tools__utils.html#a8ac4165738f6e65eb52e982389c326f9", null ],
    [ "abidiff_status_has_error", "namespaceabigail_1_1tools__utils.html#a8cfdd9d063b3aa6e56973fd9b1fe73ae", null ],
    [ "abidiff_status_has_incompatible_abi_change", "namespaceabigail_1_1tools__utils.html#a535403915032ce933ff0b80909aa03b6", null ],
    [ "base_name", "namespaceabigail_1_1tools__utils.html#a1721bbee58dc2fd2d934610d40e64197", null ],
    [ "build_corpus_group_from_kernel_dist_under", "namespaceabigail_1_1tools__utils.html#ac97a19dc8e0dad18bf8bee3076209602", null ],
    [ "check_dir", "namespaceabigail_1_1tools__utils.html#a468992b7d31139fb0eea7c819b00cc10", null ],
    [ "check_file", "namespaceabigail_1_1tools__utils.html#a00783d74dc433ebf1cb440af43a0fdbb", null ],
    [ "convert_char_stars_to_char_star_stars", "namespaceabigail_1_1tools__utils.html#ad2f37cbed85e50199ae01396c1f702ef", null ],
    [ "create_best_elf_based_reader", "namespaceabigail_1_1tools__utils.html#a54ea4c981437069b2acc6a7e96b62135", null ],
    [ "decl_names_equal", "namespaceabigail_1_1tools__utils.html#af25c37bc2c993d53de727ee81a1fd4c0", null ],
    [ "dir_contains_ctf_archive", "namespaceabigail_1_1tools__utils.html#afda1a318b1744d58fee3c3cd99078f68", null ],
    [ "dir_exists", "namespaceabigail_1_1tools__utils.html#a6bb15d5f1328eb7cb6d39674d189f535", null ],
    [ "dir_is_empty", "namespaceabigail_1_1tools__utils.html#a8709ba06d32466d1f49d657c79a6011a", null ],
    [ "dir_name", "namespaceabigail_1_1tools__utils.html#a13197cced5b826573c348a8e2e4293be", null ],
    [ "emit_prefix", "namespaceabigail_1_1tools__utils.html#a10dd5753b1b9c7fc1cb1e79e5406ef0c", null ],
    [ "ensure_dir_path_created", "namespaceabigail_1_1tools__utils.html#a633465c3104c06abcfc633ad3b6e5ce0", null ],
    [ "ensure_parent_dir_created", "namespaceabigail_1_1tools__utils.html#ac5c7cfe71379604284598f0561f35034", null ],
    [ "execute_command_and_get_output", "namespaceabigail_1_1tools__utils.html#a665c8856274a2425b423abd2106e70b9", null ],
    [ "file_exists", "namespaceabigail_1_1tools__utils.html#aa69b7cdafb964bc2d9beea6450c06708", null ],
    [ "file_has_ctf_debug_info", "namespaceabigail_1_1tools__utils.html#aaf726261fc760cc9772334ed72c2bcd3", null ],
    [ "file_has_dwarf_debug_info", "namespaceabigail_1_1tools__utils.html#a51a256306493405299ccc37b8c78d363", null ],
    [ "file_is_kernel_debuginfo_package", "namespaceabigail_1_1tools__utils.html#aeef6faf505469a785f8619cf388021bf", null ],
    [ "file_is_kernel_package", "namespaceabigail_1_1tools__utils.html#a106e6e56bd3097f28f66894744d04a8b", null ],
    [ "find_file_under_dir", "namespaceabigail_1_1tools__utils.html#a0afa089afc5059fe8ab9b0365353ad52", null ],
    [ "gen_suppr_spec_from_headers", "namespaceabigail_1_1tools__utils.html#aaeac3df45118b8d3eb2f4c4d291f6527", null ],
    [ "gen_suppr_spec_from_headers", "namespaceabigail_1_1tools__utils.html#a44b258ac19cf70473b17e78d92b2d274", null ],
    [ "gen_suppr_spec_from_headers", "namespaceabigail_1_1tools__utils.html#a062b1516279ed0ae969c2932e100b23f", null ],
    [ "gen_suppr_spec_from_kernel_abi_whitelists", "namespaceabigail_1_1tools__utils.html#a6765a9b51010d8a969ed7215eadf8e76", null ],
    [ "get_abixml_version_string", "namespaceabigail_1_1tools__utils.html#aefe29d2d70ca8aed51ca15129c9466eb", null ],
    [ "get_anonymous_enum_internal_name_prefix", "namespaceabigail_1_1tools__utils.html#a379136676607871039aa349f9bf67c53", null ],
    [ "get_anonymous_struct_internal_name_prefix", "namespaceabigail_1_1tools__utils.html#a3c397278ceb8f6e7c5eb906ee0ad9453", null ],
    [ "get_anonymous_union_internal_name_prefix", "namespaceabigail_1_1tools__utils.html#a4c1553cf7bad8ac4610be91251bc891d", null ],
    [ "get_binary_paths_from_kernel_dist", "namespaceabigail_1_1tools__utils.html#a5e240d0ccd0cfac12acffac2059b38e5", null ],
    [ "get_binary_paths_from_kernel_dist", "namespaceabigail_1_1tools__utils.html#a78ffdfb832ec7c55cf197dc1c41caa4c", null ],
    [ "get_deb_name", "namespaceabigail_1_1tools__utils.html#a926005add847902c222aff9d8cae6fcf", null ],
    [ "get_default_system_suppression_file_path", "namespaceabigail_1_1tools__utils.html#a027fdc014170c384e04f17a8ef5300ed", null ],
    [ "get_default_user_suppression_file_path", "namespaceabigail_1_1tools__utils.html#af3c2734ebfc8d68d1f03de31651c60c7", null ],
    [ "get_dsos_provided_by_rpm", "namespaceabigail_1_1tools__utils.html#a5fa09a8aa645a57ea1f4d32eb1d1eecd", null ],
    [ "get_library_version_string", "namespaceabigail_1_1tools__utils.html#a80e3f2930ea108ea1566f16c48e21b84", null ],
    [ "get_random_number", "namespaceabigail_1_1tools__utils.html#aff84ed5f64290c2c0134ebcc7b6699cb", null ],
    [ "get_random_number_as_string", "namespaceabigail_1_1tools__utils.html#af0ba6e5d6e73f8364fb8b99e9ec08dfe", null ],
    [ "get_rpm_arch", "namespaceabigail_1_1tools__utils.html#af3bccc614f971f22639e94394d880f02", null ],
    [ "get_rpm_name", "namespaceabigail_1_1tools__utils.html#a6639882eda1beaa7798c101766cce1ff", null ],
    [ "get_system_libdir", "namespaceabigail_1_1tools__utils.html#a59274813de29198cb6d69ce7feda574d", null ],
    [ "get_vmlinux_path_from_kernel_dist", "namespaceabigail_1_1tools__utils.html#a17f97b6c81b26649a9c28cc0857b7ed9", null ],
    [ "guess_file_type", "namespaceabigail_1_1tools__utils.html#a64fb191c1807b29a3c2b0aede5ce2cd4", null ],
    [ "guess_file_type", "namespaceabigail_1_1tools__utils.html#afcde84e7f80b6131261f22632e18deda", null ],
    [ "is_dir", "namespaceabigail_1_1tools__utils.html#a673ec6a3fa94cbc71c56abc7c767c37e", null ],
    [ "is_regular_file", "namespaceabigail_1_1tools__utils.html#a9fa51dd847af0637e92ee1066d29bb7b", null ],
    [ "load_default_system_suppressions", "namespaceabigail_1_1tools__utils.html#aefa1858ce86e4c2e99ecb55f0fcdb3aa", null ],
    [ "load_default_user_suppressions", "namespaceabigail_1_1tools__utils.html#a2cbe21158be5481c343c7dbd426c408a", null ],
    [ "make_path_absolute", "namespaceabigail_1_1tools__utils.html#a38dbaa65b8598c4154e6fa82ee3ed711", null ],
    [ "make_path_absolute_to_be_freed", "namespaceabigail_1_1tools__utils.html#a353279db5a15e0563c61b3511c19d292", null ],
    [ "maybe_get_symlink_target_file_path", "namespaceabigail_1_1tools__utils.html#a9899f687db84b6a8787902a248676803", null ],
    [ "operator&", "namespaceabigail_1_1tools__utils.html#ae5e2a76c6013d4ef21f2a1ffe2bf3a05", null ],
    [ "operator<<", "namespaceabigail_1_1tools__utils.html#a72cfdab590ff0bd62614f5488d1bfb33", null ],
    [ "operator|", "namespaceabigail_1_1tools__utils.html#a755b42250853b63db4243d36b93560e8", null ],
    [ "operator|=", "namespaceabigail_1_1tools__utils.html#a669cf199e370617334f8dfd3bf01f07b", null ],
    [ "real_path", "namespaceabigail_1_1tools__utils.html#ae4b5a82444383cdc2c67307bae066362", null ],
    [ "sorted_strings_common_prefix", "namespaceabigail_1_1tools__utils.html#ad97a95dc34e2b2f3f702905a9869ca07", null ],
    [ "split_string", "namespaceabigail_1_1tools__utils.html#a0684c5ea99987e521383f14caa34d5d8", null ],
    [ "string_begins_with", "namespaceabigail_1_1tools__utils.html#a4d110c3f14748246016ae15856d17fc6", null ],
    [ "string_ends_with", "namespaceabigail_1_1tools__utils.html#a0abeeb9cb2dc32575dd001abeb5d9d57", null ],
    [ "string_is_ascii", "namespaceabigail_1_1tools__utils.html#a0e2f7672ea43d8347eb6fb745134d011", null ],
    [ "string_is_ascii_identifier", "namespaceabigail_1_1tools__utils.html#a6f9d8046cc9ea4dbb4417f33e12377d2", null ],
    [ "string_suffix", "namespaceabigail_1_1tools__utils.html#aba7b8465d60c06ee2d93a5b8e254992a", null ],
    [ "trim_leading_string", "namespaceabigail_1_1tools__utils.html#af3fa11678583ff378ebbe896a353a1ca", null ],
    [ "trim_white_space", "namespaceabigail_1_1tools__utils.html#a3c3b8431527b06ea04b61e143e81a956", null ]
];