var classabigail_1_1ir_1_1type__base =
[
    [ "dynamic_hash", "structabigail_1_1ir_1_1type__base_1_1dynamic__hash.html", "structabigail_1_1ir_1_1type__base_1_1dynamic__hash" ],
    [ "hash", "structabigail_1_1ir_1_1type__base_1_1hash.html", "structabigail_1_1ir_1_1type__base_1_1hash" ],
    [ "priv", "structabigail_1_1ir_1_1type__base_1_1priv.html", "structabigail_1_1ir_1_1type__base_1_1priv" ],
    [ "shared_ptr_hash", "structabigail_1_1ir_1_1type__base_1_1shared__ptr__hash.html", null ],
    [ "type_base", "classabigail_1_1ir_1_1type__base.html#a9481422bf714fed3ff32adf2afc38588", null ],
    [ "get_alignment_in_bits", "classabigail_1_1ir_1_1type__base.html#acdee99d049775d8a164fdd48a6240f3a", null ],
    [ "get_cached_pretty_representation", "classabigail_1_1ir_1_1type__base.html#a08a531e6b60110f692ebe9d2b9fafb5d", null ],
    [ "get_canonical_type", "classabigail_1_1ir_1_1type__base.html#ae9ad4d2dffe6cf52142f4282f8ce191b", null ],
    [ "get_naked_canonical_type", "classabigail_1_1ir_1_1type__base.html#a099edffa96b517e6a41800f8760cf9ed", null ],
    [ "get_size_in_bits", "classabigail_1_1ir_1_1type__base.html#a34f8b53f2d7ed66ad1a13cf57507d8a0", null ],
    [ "on_canonical_type_set", "classabigail_1_1ir_1_1type__base.html#a7d86f4ec85b4541fac19c57df5370277", null ],
    [ "operator!=", "classabigail_1_1ir_1_1type__base.html#a89bf189bb35497ab94634c8b13a0b3e8", null ],
    [ "operator==", "classabigail_1_1ir_1_1type__base.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "set_alignment_in_bits", "classabigail_1_1ir_1_1type__base.html#ad449cd23b1d282ca2d1a147a51069716", null ],
    [ "set_size_in_bits", "classabigail_1_1ir_1_1type__base.html#a862e0347bb3d5c1c0a17e9ef79da9929", null ],
    [ "traverse", "classabigail_1_1ir_1_1type__base.html#a5f12d4bece91d0d613fc189bbca62113", null ],
    [ "canonicalize", "classabigail_1_1ir_1_1type__base.html#a7ae6d8b4d3ea344630c0cbbb7fb75364", null ]
];