var classabigail_1_1ir_1_1typedef__decl =
[
    [ "typedef_decl", "classabigail_1_1ir_1_1typedef__decl.html#a9c959c6e08da2f3ac92571d7626e4b54", null ],
    [ "typedef_decl", "classabigail_1_1ir_1_1typedef__decl.html#a9f6aaa14f4a4e878f124755117f248aa", null ],
    [ "get_alignment_in_bits", "classabigail_1_1ir_1_1typedef__decl.html#acdee99d049775d8a164fdd48a6240f3a", null ],
    [ "get_pretty_representation", "classabigail_1_1ir_1_1typedef__decl.html#aea2770952dab500dd4565fb7881411f3", null ],
    [ "get_size_in_bits", "classabigail_1_1ir_1_1typedef__decl.html#a34f8b53f2d7ed66ad1a13cf57507d8a0", null ],
    [ "get_underlying_type", "classabigail_1_1ir_1_1typedef__decl.html#ab21b00c256823331fdbc78c9913ebdce", null ],
    [ "operator==", "classabigail_1_1ir_1_1typedef__decl.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1typedef__decl.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "set_underlying_type", "classabigail_1_1ir_1_1typedef__decl.html#a3407fb92a58b64b9799ff6197d765e0c", null ],
    [ "traverse", "classabigail_1_1ir_1_1typedef__decl.html#a5f12d4bece91d0d613fc189bbca62113", null ]
];