var abg_corpus_8h =
[
    [ "corpus", "classabigail_1_1ir_1_1corpus.html", "classabigail_1_1ir_1_1corpus" ],
    [ "corpus::exported_decls_builder", "classabigail_1_1ir_1_1corpus_1_1exported__decls__builder.html", "classabigail_1_1ir_1_1corpus_1_1exported__decls__builder" ],
    [ "corpus_group", "classabigail_1_1ir_1_1corpus__group.html", "classabigail_1_1ir_1_1corpus__group" ],
    [ "operator&", "abg-corpus_8h.html#a86a7158049ba82358bb83a451a3d7f21", null ],
    [ "operator&=", "abg-corpus_8h.html#adcc13be6bf586149341b6d46e52bb5fd", null ],
    [ "operator|", "abg-corpus_8h.html#a795a0d11a1ffa5f454c963c7287ec784", null ],
    [ "operator|=", "abg-corpus_8h.html#a2256497a643a8b6d3f8ae571f12a14bb", null ]
];