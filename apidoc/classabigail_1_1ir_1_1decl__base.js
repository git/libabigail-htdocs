var classabigail_1_1ir_1_1decl__base =
[
    [ "binding", "classabigail_1_1ir_1_1decl__base.html#a32566dc1248e088926a64a122ce142cc", [
      [ "BINDING_NONE", "classabigail_1_1ir_1_1decl__base.html#a32566dc1248e088926a64a122ce142ccad1f97ab5cf11c1811cafde703ed7d5ac", null ],
      [ "BINDING_LOCAL", "classabigail_1_1ir_1_1decl__base.html#a32566dc1248e088926a64a122ce142cca15063a12283f67293a8d185374e6b8aa", null ],
      [ "BINDING_GLOBAL", "classabigail_1_1ir_1_1decl__base.html#a32566dc1248e088926a64a122ce142ccaaa5e5b906eba415dc823319fe56729e7", null ],
      [ "BINDING_WEAK", "classabigail_1_1ir_1_1decl__base.html#a32566dc1248e088926a64a122ce142cca5bc7b69afc21dd2d8912b61172beb544", null ]
    ] ],
    [ "visibility", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461b", [
      [ "VISIBILITY_NONE", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461ba831590a83409cdd9772ab5f1d63336c4", null ],
      [ "VISIBILITY_DEFAULT", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461ba1e2ac0e3b4f1a2ebe6b16f7d6c0a1ddd", null ],
      [ "VISIBILITY_PROTECTED", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461baec13791aa1b77620460c9c9510732dec", null ],
      [ "VISIBILITY_HIDDEN", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461ba0837355274617d495f9f984d9c4638cb", null ],
      [ "VISIBILITY_INTERNAL", "classabigail_1_1ir_1_1decl__base.html#a9c19c0281692cea55c1c050a3d9d461ba2578f0aa1b4c9248194cd33dbc7dbb2e", null ]
    ] ],
    [ "decl_base", "classabigail_1_1ir_1_1decl__base.html#ada0e1372b4c9f0306094291a31552220", null ],
    [ "decl_base", "classabigail_1_1ir_1_1decl__base.html#acc546fca294926e598ca939d6a166daf", null ],
    [ "decl_base", "classabigail_1_1ir_1_1decl__base.html#a6917e3a23e9e046f772d3360b2b0f760", null ],
    [ "~decl_base", "classabigail_1_1ir_1_1decl__base.html#aba4eabf27d2751f06342fe93c7b18067", null ],
    [ "clear_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a2bc89fc8f7ccb278898d1b4a6ef9a5c9", null ],
    [ "get_context_rel", "classabigail_1_1ir_1_1decl__base.html#a7d26d5a998c1551776c011ef1b2e4733", null ],
    [ "get_context_rel", "classabigail_1_1ir_1_1decl__base.html#a47bd832cc33754c86f2867da717d27d3", null ],
    [ "get_definition_of_declaration", "classabigail_1_1ir_1_1decl__base.html#a6c4a5483604442d8ed4c40f2ab44455e", null ],
    [ "get_earlier_declaration", "classabigail_1_1ir_1_1decl__base.html#a15ad17796ea772f9e9702312c4b7aee9", null ],
    [ "get_has_anonymous_parent", "classabigail_1_1ir_1_1decl__base.html#ace5403d7d117f55ae9c3edcfc76a9185", null ],
    [ "get_hash", "classabigail_1_1ir_1_1decl__base.html#ae1ac2018a4646d71125fa5473670aea8", null ],
    [ "get_is_anonymous", "classabigail_1_1ir_1_1decl__base.html#a58ea89d2339a06f1d1aecaecd3e81c17", null ],
    [ "get_is_anonymous_or_has_anonymous_parent", "classabigail_1_1ir_1_1decl__base.html#ac9ba36b214b2137aed04c2baa6c3e90e", null ],
    [ "get_is_declaration_only", "classabigail_1_1ir_1_1decl__base.html#a9cedd0b5f6b07c6c2f37e1b0da7e3506", null ],
    [ "get_is_in_public_symbol_table", "classabigail_1_1ir_1_1decl__base.html#ad60debd86500be315b0f6e7ad1ee6949", null ],
    [ "get_linkage_name", "classabigail_1_1ir_1_1decl__base.html#aa50a8154f37b8078528b46b672681959", null ],
    [ "get_location", "classabigail_1_1ir_1_1decl__base.html#a2ede9671b8238ef196257ae98ba79f4e", null ],
    [ "get_naked_definition_of_declaration", "classabigail_1_1ir_1_1decl__base.html#a25174e4b13670214120089b331cbfe3d", null ],
    [ "get_name", "classabigail_1_1ir_1_1decl__base.html#a3bf6e943e806764b96f32a3ab81a0bf0", null ],
    [ "get_naming_typedef", "classabigail_1_1ir_1_1decl__base.html#a36d476fd8d0391d92800eb737b2888b2", null ],
    [ "get_pretty_representation", "classabigail_1_1ir_1_1decl__base.html#aea2770952dab500dd4565fb7881411f3", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a48d1909c881954a9ffe4efd640625196", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a29015ca799ed6bc508517123347e0958", null ],
    [ "get_qualified_parent_name", "classabigail_1_1ir_1_1decl__base.html#ac50d42f1c32274ddb64bccc5eecfaa8f", null ],
    [ "get_scope", "classabigail_1_1ir_1_1decl__base.html#a0b689fbe51317bd94ec22bb951585e6d", null ],
    [ "get_scoped_name", "classabigail_1_1ir_1_1decl__base.html#a68004c2d68d16ef56d446ae35ae8721e", null ],
    [ "get_visibility", "classabigail_1_1ir_1_1decl__base.html#a9bf170eda6107b0a52025fd0460b05ad", null ],
    [ "operator!=", "classabigail_1_1ir_1_1decl__base.html#a0723233747b59429dce1db92303e84a7", null ],
    [ "operator==", "classabigail_1_1ir_1_1decl__base.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "peek_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a41e019ba7322f53d257edfe55b403f36", null ],
    [ "peek_temporary_qualified_name", "classabigail_1_1ir_1_1decl__base.html#ae653a693e17fee59adf297ab8ba9fcb3", null ],
    [ "set_definition_of_declaration", "classabigail_1_1ir_1_1decl__base.html#a03c02c720d424620b8f94b10adc6ca4a", null ],
    [ "set_earlier_declaration", "classabigail_1_1ir_1_1decl__base.html#aa3dbdfb1da19503b7fd312cc2a705b5e", null ],
    [ "set_is_anonymous", "classabigail_1_1ir_1_1decl__base.html#a86b141f20f56c3d95fb8d67ef1c88fe4", null ],
    [ "set_is_declaration_only", "classabigail_1_1ir_1_1decl__base.html#a05a6618343222f0a37cf07be7066c537", null ],
    [ "set_is_in_public_symbol_table", "classabigail_1_1ir_1_1decl__base.html#a0e369a544fecea14d74c0e51429a976c", null ],
    [ "set_linkage_name", "classabigail_1_1ir_1_1decl__base.html#a1d639cedc89b05fe744584003c37e62b", null ],
    [ "set_location", "classabigail_1_1ir_1_1decl__base.html#a80c1cab17f0d965f0930d520bcaf0757", null ],
    [ "set_name", "classabigail_1_1ir_1_1decl__base.html#a2bf8ca936b1ee562b5b534332e9753ae", null ],
    [ "set_naming_typedef", "classabigail_1_1ir_1_1decl__base.html#a7f81a929073faa05820678f197d934a7", null ],
    [ "set_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a0c7a7f408d4de8e762fd9329e28eafed", null ],
    [ "set_scope", "classabigail_1_1ir_1_1decl__base.html#a3da7273df2a88db481d983f4a938a27e", null ],
    [ "set_temporary_qualified_name", "classabigail_1_1ir_1_1decl__base.html#a91c063cfe83dff7033e9bc8eb5012669", null ],
    [ "set_visibility", "classabigail_1_1ir_1_1decl__base.html#a8969fa7c79f5cdcb36952bd34cee487d", null ],
    [ "traverse", "classabigail_1_1ir_1_1decl__base.html#aa0435efbde239af376b5d0dfa16135d2", null ],
    [ "add_decl_to_scope", "classabigail_1_1ir_1_1decl__base.html#a64d9b92ba0fdc73e6cbe6d8955d3925b", null ],
    [ "canonicalize", "classabigail_1_1ir_1_1decl__base.html#a7ae6d8b4d3ea344630c0cbbb7fb75364", null ],
    [ "equals", "classabigail_1_1ir_1_1decl__base.html#add1b2fbcd869757009a1aa18a1b2c257", null ],
    [ "equals", "classabigail_1_1ir_1_1decl__base.html#aaca4ff0e0a8eb682eefca045293c7e7c", null ],
    [ "get_member_access_specifier", "classabigail_1_1ir_1_1decl__base.html#aaf866cb6b5226d82b2e80301c6da4e03", null ],
    [ "get_member_access_specifier", "classabigail_1_1ir_1_1decl__base.html#ac336c35e6dbae21deba8ab2de558d402", null ],
    [ "get_member_function_is_virtual", "classabigail_1_1ir_1_1decl__base.html#ab76197f4e5618c511f0ca539eb1ef970", null ],
    [ "get_member_is_static", "classabigail_1_1ir_1_1decl__base.html#a13ffe6f2525a597d24f77b6d4be1f8ee", null ],
    [ "get_member_is_static", "classabigail_1_1ir_1_1decl__base.html#ab433fee74694bf21b3c556ae304d0531", null ],
    [ "maybe_compare_as_member_decls", "classabigail_1_1ir_1_1decl__base.html#af55062565bdfd354a59b9530552a5e30", null ],
    [ "remove_decl_from_scope", "classabigail_1_1ir_1_1decl__base.html#a74d1e28a1aed78d9174f576d1d3c7542", null ],
    [ "set_member_access_specifier", "classabigail_1_1ir_1_1decl__base.html#a6f11c7fd70c027c0e6463ca4efcbb5bc", null ],
    [ "set_member_function_is_virtual", "classabigail_1_1ir_1_1decl__base.html#a4fe48cee1d87eaec7aa88f8475b7e35f", null ],
    [ "set_member_is_static", "classabigail_1_1ir_1_1decl__base.html#a670c37aa428f7ad1751603f783780b39", null ],
    [ "set_member_is_static", "classabigail_1_1ir_1_1decl__base.html#ac80e7df10f93c869ce91f3580333393b", null ]
];