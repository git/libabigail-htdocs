var namespaceabigail_1_1elf =
[
    [ "reader", "classabigail_1_1elf_1_1reader.html", "classabigail_1_1elf_1_1reader" ],
    [ "reader_sptr", "namespaceabigail_1_1elf.html#a99185c8e2b8bb4694d8430c6f87ca71b", null ],
    [ "elf_type", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036", [
      [ "ELF_TYPE_EXEC", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036acf1893f4e0fa920c9d0d425508b2853a", null ],
      [ "ELF_TYPE_PI_EXEC", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036a73aa9d00c35bb7edef1bdeccb97b0183", null ],
      [ "ELF_TYPE_DSO", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036ae8b517dfcaf6759ed072be26bb25717b", null ],
      [ "ELF_TYPE_RELOCATABLE", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036a2bb31490b35a6dcef153cb889b6b43bc", null ],
      [ "ELF_TYPE_UNKNOWN", "namespaceabigail_1_1elf.html#aac6179d8ce27f14e3ea093b62a61e036a5648a19b1e2a23f7c79d21fbee873e7f", null ]
    ] ],
    [ "get_soname_of_elf_file", "namespaceabigail_1_1elf.html#a5931f990bd21bf4c6e08a7e47e29f710", null ],
    [ "get_type_of_elf_file", "namespaceabigail_1_1elf.html#ae1a0b787978d575a5977456c3530d6fe", null ]
];