var structabigail_1_1ir_1_1corpus_1_1priv =
[
    [ "~priv", "structabigail_1_1ir_1_1corpus_1_1priv.html#ac1947dade433b45b18a7b14fa9b0e375", null ],
    [ "get_fun_symbol_map", "structabigail_1_1ir_1_1corpus_1_1priv.html#a7ca17329da42867fbb6b3f8fcc722364", null ],
    [ "get_public_types_pretty_representations", "structabigail_1_1ir_1_1corpus_1_1priv.html#a236be40bc92471b7d11a6733c94790b7", null ],
    [ "get_sorted_fun_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#a46e390f5b16f583b63df4a987fd2a442", null ],
    [ "get_sorted_undefined_fun_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#a63eed5af1fb33eb589287c3183efa86c", null ],
    [ "get_sorted_undefined_var_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#ab464b837c9ae40bd2bc46e9e16201a65", null ],
    [ "get_sorted_var_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#a04effa76f91111288c4da656c7034cf0", null ],
    [ "get_types", "structabigail_1_1ir_1_1corpus_1_1priv.html#a5c10be814113300ee6b389bde642efb4", null ],
    [ "get_types", "structabigail_1_1ir_1_1corpus_1_1priv.html#af5a72675a11839432974d7308964af5f", null ],
    [ "get_undefined_fun_symbol_map", "structabigail_1_1ir_1_1corpus_1_1priv.html#a72b4782f88173b7ddcde4cfe666e1e9c", null ],
    [ "get_undefined_var_symbol_map", "structabigail_1_1ir_1_1corpus_1_1priv.html#a1d198c565f35708970b189c8efce02ed", null ],
    [ "get_unreferenced_function_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#a91bd025a7bb1e3790ea43544f12714bf", null ],
    [ "get_unreferenced_variable_symbols", "structabigail_1_1ir_1_1corpus_1_1priv.html#a9821be0e83d9b194a82a8869b3410898", null ],
    [ "get_var_symbol_map", "structabigail_1_1ir_1_1corpus_1_1priv.html#a30df94954aadd418e2b0fb0a27880834", null ]
];