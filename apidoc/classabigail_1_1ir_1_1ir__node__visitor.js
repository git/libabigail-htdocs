var classabigail_1_1ir_1_1ir__node__visitor =
[
    [ "ir_node_visitor", "classabigail_1_1ir_1_1ir__node__visitor.html#a91027742669cae52bdc6f35232324bec", null ],
    [ "allow_visiting_already_visited_type_node", "classabigail_1_1ir_1_1ir__node__visitor.html#a3b7044a5f6f0140d3027768501dc84c6", null ],
    [ "allow_visiting_already_visited_type_node", "classabigail_1_1ir_1_1ir__node__visitor.html#a52adb33435e79f864d7095a0fcee944f", null ],
    [ "forget_visited_type_nodes", "classabigail_1_1ir_1_1ir__node__visitor.html#a4c214ad6e9a1f422ac2b9de57a36186b", null ],
    [ "mark_type_node_as_visited", "classabigail_1_1ir_1_1ir__node__visitor.html#aac69279da1fd08d5e3ee60def1dbc103", null ],
    [ "type_node_has_been_visited", "classabigail_1_1ir_1_1ir__node__visitor.html#a403b5518f8ca942dad39193f6bd41554", null ]
];