var hierarchy =
[
    [ "optional< T >", "classabg__compat_1_1optional.html", null ],
    [ "canvas", "structabigail_1_1canvas.html", null ],
    [ "array_diff::priv", "structabigail_1_1comparison_1_1array__diff_1_1priv.html", null ],
    [ "base_diff::priv", "structabigail_1_1comparison_1_1base__diff_1_1priv.html", null ],
    [ "base_diff_comp", "structabigail_1_1comparison_1_1base__diff__comp.html", null ],
    [ "base_spec_comp", "structabigail_1_1comparison_1_1base__spec__comp.html", null ],
    [ "changed_enumerator_comp", "structabigail_1_1comparison_1_1changed__enumerator__comp.html", null ],
    [ "class_diff::priv", "structabigail_1_1comparison_1_1class__diff_1_1priv.html", null ],
    [ "class_or_union_diff::priv", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html", null ],
    [ "corpus_diff", "classabigail_1_1comparison_1_1corpus__diff.html", null ],
    [ "corpus_diff::diff_stats", "classabigail_1_1comparison_1_1corpus__diff_1_1diff__stats.html", null ],
    [ "corpus_diff::diff_stats::priv", "structabigail_1_1comparison_1_1corpus__diff_1_1diff__stats_1_1priv.html", null ],
    [ "corpus_diff::priv", "structabigail_1_1comparison_1_1corpus__diff_1_1priv.html", null ],
    [ "data_member_comp", "structabigail_1_1comparison_1_1data__member__comp.html", null ],
    [ "data_member_diff_comp", "structabigail_1_1comparison_1_1data__member__diff__comp.html", null ],
    [ "decl_diff_base::priv", "structabigail_1_1comparison_1_1decl__diff__base_1_1priv.html", null ],
    [ "diff::priv", "structabigail_1_1comparison_1_1diff_1_1priv.html", null ],
    [ "diff_comp", "structabigail_1_1comparison_1_1diff__comp.html", null ],
    [ "diff_context", "classabigail_1_1comparison_1_1diff__context.html", null ],
    [ "diff_context::priv", "structabigail_1_1comparison_1_1diff__context_1_1priv.html", null ],
    [ "diff_equal", "structabigail_1_1comparison_1_1diff__equal.html", null ],
    [ "diff_hash", "structabigail_1_1comparison_1_1diff__hash.html", null ],
    [ "diff_less_than_functor", "structabigail_1_1comparison_1_1diff__less__than__functor.html", null ],
    [ "diff_maps", "classabigail_1_1comparison_1_1diff__maps.html", null ],
    [ "diff_sptr_hasher", "structabigail_1_1comparison_1_1diff__sptr__hasher.html", null ],
    [ "distinct_diff::priv", "structabigail_1_1comparison_1_1distinct__diff_1_1priv.html", null ],
    [ "elf_symbol_comp", "structabigail_1_1comparison_1_1elf__symbol__comp.html", null ],
    [ "enum_diff::priv", "structabigail_1_1comparison_1_1enum__diff_1_1priv.html", null ],
    [ "enumerator_value_comp", "structabigail_1_1comparison_1_1enumerator__value__comp.html", null ],
    [ "fn_parm_diff::priv", "structabigail_1_1comparison_1_1fn__parm__diff_1_1priv.html", null ],
    [ "fn_parm_diff_comp", "structabigail_1_1comparison_1_1fn__parm__diff__comp.html", null ],
    [ "function_comp", "structabigail_1_1comparison_1_1function__comp.html", null ],
    [ "function_decl_diff::priv", "structabigail_1_1comparison_1_1function__decl__diff_1_1priv.html", null ],
    [ "function_decl_diff_comp", "structabigail_1_1comparison_1_1function__decl__diff__comp.html", null ],
    [ "function_type_diff::priv", "structabigail_1_1comparison_1_1function__type__diff_1_1priv.html", null ],
    [ "parm_comp", "structabigail_1_1comparison_1_1parm__comp.html", null ],
    [ "pointer_diff::priv", "structabigail_1_1comparison_1_1pointer__diff_1_1priv.html", null ],
    [ "qualified_type_diff::priv", "structabigail_1_1comparison_1_1qualified__type__diff_1_1priv.html", null ],
    [ "reference_diff::priv", "structabigail_1_1comparison_1_1reference__diff_1_1priv.html", null ],
    [ "reporter_base", "classabigail_1_1comparison_1_1reporter__base.html", [
      [ "default_reporter", "classabigail_1_1comparison_1_1default__reporter.html", [
        [ "leaf_reporter", "classabigail_1_1comparison_1_1leaf__reporter.html", null ]
      ] ]
    ] ],
    [ "scope_diff::priv", "structabigail_1_1comparison_1_1scope__diff_1_1priv.html", null ],
    [ "translation_unit_diff::priv", "structabigail_1_1comparison_1_1translation__unit__diff_1_1priv.html", null ],
    [ "type_diff_base::priv", "structabigail_1_1comparison_1_1type__diff__base_1_1priv.html", null ],
    [ "typedef_diff::priv", "structabigail_1_1comparison_1_1typedef__diff_1_1priv.html", null ],
    [ "types_or_decls_equal", "structabigail_1_1comparison_1_1types__or__decls__equal.html", null ],
    [ "types_or_decls_hash", "structabigail_1_1comparison_1_1types__or__decls__hash.html", null ],
    [ "var_comp", "structabigail_1_1comparison_1_1var__comp.html", null ],
    [ "var_diff::priv", "structabigail_1_1comparison_1_1var__diff_1_1priv.html", null ],
    [ "var_diff_sptr_comp", "structabigail_1_1comparison_1_1var__diff__sptr__comp.html", null ],
    [ "virtual_member_function_diff_comp", "structabigail_1_1comparison_1_1virtual__member__function__diff__comp.html", null ],
    [ "config", "classabigail_1_1config.html", null ],
    [ "deep_ptr_eq_functor", "structabigail_1_1diff__utils_1_1deep__ptr__eq__functor.html", null ],
    [ "default_eq_functor", "structabigail_1_1diff__utils_1_1default__eq__functor.html", null ],
    [ "deletion", "classabigail_1_1diff__utils_1_1deletion.html", null ],
    [ "edit_script", "classabigail_1_1diff__utils_1_1edit__script.html", null ],
    [ "insertion", "classabigail_1_1diff__utils_1_1insertion.html", null ],
    [ "point", "classabigail_1_1diff__utils_1_1point.html", null ],
    [ "snake", "classabigail_1_1diff__utils_1_1snake.html", null ],
    [ "dot", "structabigail_1_1dot.html", null ],
    [ "dwfl_deleter", "structabigail_1_1elf__helpers_1_1dwfl__deleter.html", null ],
    [ "fe_iface", "classabigail_1_1fe__iface.html", [
      [ "reader", "classabigail_1_1elf_1_1reader.html", [
        [ "elf_based_reader", "classabigail_1_1elf__based__reader.html", null ]
      ] ]
    ] ],
    [ "fe_iface::options_type", "structabigail_1_1fe__iface_1_1options__type.html", null ],
    [ "hash_interned_string", "structabigail_1_1hash__interned__string.html", null ],
    [ "config", "classabigail_1_1ini_1_1config.html", null ],
    [ "config::section", "classabigail_1_1ini_1_1config_1_1section.html", null ],
    [ "function_call_expr", "classabigail_1_1ini_1_1function__call__expr.html", null ],
    [ "property", "classabigail_1_1ini_1_1property.html", [
      [ "list_property", "classabigail_1_1ini_1_1list__property.html", null ],
      [ "simple_property", "classabigail_1_1ini_1_1simple__property.html", null ],
      [ "tuple_property", "classabigail_1_1ini_1_1tuple__property.html", null ]
    ] ],
    [ "property_value", "classabigail_1_1ini_1_1property__value.html", [
      [ "list_property_value", "classabigail_1_1ini_1_1list__property__value.html", null ],
      [ "string_property_value", "classabigail_1_1ini_1_1string__property__value.html", null ],
      [ "tuple_property_value", "classabigail_1_1ini_1_1tuple__property__value.html", null ]
    ] ],
    [ "interned_string", "classabigail_1_1interned__string.html", null ],
    [ "interned_string_pool", "classabigail_1_1interned__string__pool.html", null ],
    [ "array_type_def::subrange_type::bound_value", "classabigail_1_1ir_1_1array__type__def_1_1subrange__type_1_1bound__value.html", null ],
    [ "canonical_type_hash", "structabigail_1_1ir_1_1canonical__type__hash.html", null ],
    [ "class_decl::base_spec::hash", "structabigail_1_1ir_1_1class__decl_1_1base__spec_1_1hash.html", null ],
    [ "class_decl::hash", "structabigail_1_1ir_1_1class__decl_1_1hash.html", null ],
    [ "class_or_union::hash", "structabigail_1_1ir_1_1class__or__union_1_1hash.html", null ],
    [ "class_or_union::priv", "structabigail_1_1ir_1_1class__or__union_1_1priv.html", null ],
    [ "class_tdecl::hash", "structabigail_1_1ir_1_1class__tdecl_1_1hash.html", null ],
    [ "class_tdecl::shared_ptr_hash", "structabigail_1_1ir_1_1class__tdecl_1_1shared__ptr__hash.html", null ],
    [ "context_rel", "classabigail_1_1ir_1_1context__rel.html", [
      [ "dm_context_rel", "classabigail_1_1ir_1_1dm__context__rel.html", null ],
      [ "mem_fn_context_rel", "classabigail_1_1ir_1_1mem__fn__context__rel.html", null ]
    ] ],
    [ "corpus", "classabigail_1_1ir_1_1corpus.html", [
      [ "corpus_group", "classabigail_1_1ir_1_1corpus__group.html", null ]
    ] ],
    [ "corpus::exported_decls_builder", "classabigail_1_1ir_1_1corpus_1_1exported__decls__builder.html", null ],
    [ "corpus::exported_decls_builder::priv", "classabigail_1_1ir_1_1corpus_1_1exported__decls__builder_1_1priv.html", null ],
    [ "corpus::priv", "structabigail_1_1ir_1_1corpus_1_1priv.html", null ],
    [ "elf_symbol", "classabigail_1_1ir_1_1elf__symbol.html", null ],
    [ "elf_symbol::version", "classabigail_1_1ir_1_1elf__symbol_1_1version.html", null ],
    [ "enum_type_decl::enumerator", "classabigail_1_1ir_1_1enum__type__decl_1_1enumerator.html", null ],
    [ "environment", "classabigail_1_1ir_1_1environment.html", null ],
    [ "environment::priv", "structabigail_1_1ir_1_1environment_1_1priv.html", null ],
    [ "function_decl::hash", "structabigail_1_1ir_1_1function__decl_1_1hash.html", null ],
    [ "function_decl::parameter::hash", "structabigail_1_1ir_1_1function__decl_1_1parameter_1_1hash.html", null ],
    [ "function_decl::ptr_equal", "structabigail_1_1ir_1_1function__decl_1_1ptr__equal.html", null ],
    [ "function_tdecl::hash", "structabigail_1_1ir_1_1function__tdecl_1_1hash.html", null ],
    [ "function_tdecl::shared_ptr_hash", "structabigail_1_1ir_1_1function__tdecl_1_1shared__ptr__hash.html", null ],
    [ "function_type::hash", "structabigail_1_1ir_1_1function__type_1_1hash.html", null ],
    [ "function_type::priv", "structabigail_1_1ir_1_1function__type_1_1priv.html", null ],
    [ "integral_type", "classabigail_1_1ir_1_1integral__type.html", null ],
    [ "location", "classabigail_1_1ir_1_1location.html", null ],
    [ "location_manager", "classabigail_1_1ir_1_1location__manager.html", null ],
    [ "member_base", "classabigail_1_1ir_1_1member__base.html", [
      [ "class_decl::base_spec", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html", null ],
      [ "member_class_template", "classabigail_1_1ir_1_1member__class__template.html", null ],
      [ "member_function_template", "classabigail_1_1ir_1_1member__function__template.html", null ]
    ] ],
    [ "member_base::hash", "structabigail_1_1ir_1_1member__base_1_1hash.html", null ],
    [ "member_class_template::hash", "structabigail_1_1ir_1_1member__class__template_1_1hash.html", null ],
    [ "member_function_template::hash", "structabigail_1_1ir_1_1member__function__template_1_1hash.html", null ],
    [ "node_visitor_base", "structabigail_1_1ir_1_1node__visitor__base.html", [
      [ "diff_node_visitor", "classabigail_1_1comparison_1_1diff__node__visitor.html", [
        [ "filter_base", "structabigail_1_1comparison_1_1filtering_1_1filter__base.html", [
          [ "harmless_filter", "classabigail_1_1comparison_1_1filtering_1_1harmless__filter.html", null ],
          [ "harmless_harmful_filter", "classabigail_1_1comparison_1_1filtering_1_1harmless__harmful__filter.html", null ]
        ] ]
      ] ],
      [ "ir_node_visitor", "classabigail_1_1ir_1_1ir__node__visitor.html", null ]
    ] ],
    [ "non_type_tparameter::hash", "structabigail_1_1ir_1_1non__type__tparameter_1_1hash.html", null ],
    [ "scope_decl::hash", "structabigail_1_1ir_1_1scope__decl_1_1hash.html", null ],
    [ "shared_translation_unit_comp", "structabigail_1_1ir_1_1shared__translation__unit__comp.html", null ],
    [ "template_decl::hash", "structabigail_1_1ir_1_1template__decl_1_1hash.html", null ],
    [ "template_parameter", "classabigail_1_1ir_1_1template__parameter.html", [
      [ "non_type_tparameter", "classabigail_1_1ir_1_1non__type__tparameter.html", null ],
      [ "type_composition", "classabigail_1_1ir_1_1type__composition.html", null ],
      [ "type_tparameter", "classabigail_1_1ir_1_1type__tparameter.html", [
        [ "template_tparameter", "classabigail_1_1ir_1_1template__tparameter.html", null ]
      ] ]
    ] ],
    [ "translation_unit::priv", "structabigail_1_1ir_1_1translation__unit_1_1priv.html", null ],
    [ "traversable_base", "classabigail_1_1ir_1_1traversable__base.html", [
      [ "diff_traversable_base", "classabigail_1_1comparison_1_1diff__traversable__base.html", [
        [ "diff", "classabigail_1_1comparison_1_1diff.html", [
          [ "base_diff", "classabigail_1_1comparison_1_1base__diff.html", null ],
          [ "decl_diff_base", "classabigail_1_1comparison_1_1decl__diff__base.html", [
            [ "fn_parm_diff", "classabigail_1_1comparison_1_1fn__parm__diff.html", null ],
            [ "function_decl_diff", "classabigail_1_1comparison_1_1function__decl__diff.html", null ],
            [ "var_diff", "classabigail_1_1comparison_1_1var__diff.html", null ]
          ] ],
          [ "distinct_diff", "classabigail_1_1comparison_1_1distinct__diff.html", null ],
          [ "scope_diff", "classabigail_1_1comparison_1_1scope__diff.html", [
            [ "translation_unit_diff", "classabigail_1_1comparison_1_1translation__unit__diff.html", null ]
          ] ],
          [ "type_diff_base", "classabigail_1_1comparison_1_1type__diff__base.html", [
            [ "array_diff", "classabigail_1_1comparison_1_1array__diff.html", null ],
            [ "class_or_union_diff", "classabigail_1_1comparison_1_1class__or__union__diff.html", [
              [ "class_diff", "classabigail_1_1comparison_1_1class__diff.html", null ],
              [ "union_diff", "classabigail_1_1comparison_1_1union__diff.html", null ]
            ] ],
            [ "enum_diff", "classabigail_1_1comparison_1_1enum__diff.html", null ],
            [ "function_type_diff", "classabigail_1_1comparison_1_1function__type__diff.html", null ],
            [ "pointer_diff", "classabigail_1_1comparison_1_1pointer__diff.html", null ],
            [ "qualified_type_diff", "classabigail_1_1comparison_1_1qualified__type__diff.html", null ],
            [ "reference_diff", "classabigail_1_1comparison_1_1reference__diff.html", null ],
            [ "type_decl_diff", "classabigail_1_1comparison_1_1type__decl__diff.html", null ],
            [ "typedef_diff", "classabigail_1_1comparison_1_1typedef__diff.html", null ]
          ] ]
        ] ]
      ] ],
      [ "ir_traversable_base", "structabigail_1_1ir_1_1ir__traversable__base.html", [
        [ "type_or_decl_base", "classabigail_1_1ir_1_1type__or__decl__base.html", [
          [ "decl_base", "classabigail_1_1ir_1_1decl__base.html", [
            [ "array_type_def", "classabigail_1_1ir_1_1array__type__def.html", null ],
            [ "array_type_def::subrange_type", "classabigail_1_1ir_1_1array__type__def_1_1subrange__type.html", null ],
            [ "class_decl::base_spec", "classabigail_1_1ir_1_1class__decl_1_1base__spec.html", null ],
            [ "enum_type_decl", "classabigail_1_1ir_1_1enum__type__decl.html", null ],
            [ "function_decl", "classabigail_1_1ir_1_1function__decl.html", [
              [ "method_decl", "classabigail_1_1ir_1_1method__decl.html", null ]
            ] ],
            [ "function_decl::parameter", "classabigail_1_1ir_1_1function__decl_1_1parameter.html", null ],
            [ "member_class_template", "classabigail_1_1ir_1_1member__class__template.html", null ],
            [ "member_function_template", "classabigail_1_1ir_1_1member__function__template.html", null ],
            [ "non_type_tparameter", "classabigail_1_1ir_1_1non__type__tparameter.html", null ],
            [ "pointer_type_def", "classabigail_1_1ir_1_1pointer__type__def.html", null ],
            [ "qualified_type_def", "classabigail_1_1ir_1_1qualified__type__def.html", null ],
            [ "reference_type_def", "classabigail_1_1ir_1_1reference__type__def.html", null ],
            [ "scope_decl", "classabigail_1_1ir_1_1scope__decl.html", [
              [ "class_tdecl", "classabigail_1_1ir_1_1class__tdecl.html", null ],
              [ "function_tdecl", "classabigail_1_1ir_1_1function__tdecl.html", null ],
              [ "global_scope", "classabigail_1_1ir_1_1global__scope.html", null ],
              [ "namespace_decl", "classabigail_1_1ir_1_1namespace__decl.html", null ],
              [ "scope_type_decl", "classabigail_1_1ir_1_1scope__type__decl.html", [
                [ "class_or_union", "classabigail_1_1ir_1_1class__or__union.html", [
                  [ "class_decl", "classabigail_1_1ir_1_1class__decl.html", null ],
                  [ "union_decl", "classabigail_1_1ir_1_1union__decl.html", null ]
                ] ]
              ] ]
            ] ],
            [ "template_decl", "classabigail_1_1ir_1_1template__decl.html", [
              [ "class_tdecl", "classabigail_1_1ir_1_1class__tdecl.html", null ],
              [ "function_tdecl", "classabigail_1_1ir_1_1function__tdecl.html", null ],
              [ "template_tparameter", "classabigail_1_1ir_1_1template__tparameter.html", null ]
            ] ],
            [ "type_composition", "classabigail_1_1ir_1_1type__composition.html", null ],
            [ "type_decl", "classabigail_1_1ir_1_1type__decl.html", [
              [ "type_tparameter", "classabigail_1_1ir_1_1type__tparameter.html", null ]
            ] ],
            [ "typedef_decl", "classabigail_1_1ir_1_1typedef__decl.html", null ],
            [ "var_decl", "classabigail_1_1ir_1_1var__decl.html", null ]
          ] ],
          [ "type_base", "classabigail_1_1ir_1_1type__base.html", [
            [ "array_type_def", "classabigail_1_1ir_1_1array__type__def.html", null ],
            [ "array_type_def::subrange_type", "classabigail_1_1ir_1_1array__type__def_1_1subrange__type.html", null ],
            [ "enum_type_decl", "classabigail_1_1ir_1_1enum__type__decl.html", null ],
            [ "function_type", "classabigail_1_1ir_1_1function__type.html", [
              [ "method_type", "classabigail_1_1ir_1_1method__type.html", null ]
            ] ],
            [ "pointer_type_def", "classabigail_1_1ir_1_1pointer__type__def.html", null ],
            [ "qualified_type_def", "classabigail_1_1ir_1_1qualified__type__def.html", null ],
            [ "reference_type_def", "classabigail_1_1ir_1_1reference__type__def.html", null ],
            [ "scope_type_decl", "classabigail_1_1ir_1_1scope__type__decl.html", null ],
            [ "type_decl", "classabigail_1_1ir_1_1type__decl.html", null ],
            [ "typedef_decl", "classabigail_1_1ir_1_1typedef__decl.html", null ]
          ] ]
        ] ]
      ] ],
      [ "translation_unit", "classabigail_1_1ir_1_1translation__unit.html", null ]
    ] ],
    [ "type_base::dynamic_hash", "structabigail_1_1ir_1_1type__base_1_1dynamic__hash.html", null ],
    [ "type_base::hash", "structabigail_1_1ir_1_1type__base_1_1hash.html", null ],
    [ "type_base::priv", "structabigail_1_1ir_1_1type__base_1_1priv.html", null ],
    [ "type_base::shared_ptr_hash", "structabigail_1_1ir_1_1type__base_1_1shared__ptr__hash.html", null ],
    [ "type_composition::hash", "structabigail_1_1ir_1_1type__composition_1_1hash.html", null ],
    [ "type_maps", "classabigail_1_1ir_1_1type__maps.html", null ],
    [ "type_or_decl_base_comp", "structabigail_1_1ir_1_1type__or__decl__base__comp.html", null ],
    [ "type_or_decl_equal", "structabigail_1_1ir_1_1type__or__decl__equal.html", null ],
    [ "type_or_decl_hash", "structabigail_1_1ir_1_1type__or__decl__hash.html", null ],
    [ "type_ptr_equal", "structabigail_1_1ir_1_1type__ptr__equal.html", null ],
    [ "type_shared_ptr_equal", "structabigail_1_1ir_1_1type__shared__ptr__equal.html", null ],
    [ "uint64_t_pair_hash", "structabigail_1_1ir_1_1uint64__t__pair__hash.html", null ],
    [ "var_decl::hash", "structabigail_1_1ir_1_1var__decl_1_1hash.html", null ],
    [ "var_decl::ptr_equal", "structabigail_1_1ir_1_1var__decl_1_1ptr__equal.html", null ],
    [ "node_base", "structabigail_1_1node__base.html", [
      [ "child_node", "structabigail_1_1child__node.html", null ],
      [ "parent_node", "structabigail_1_1parent__node.html", null ]
    ] ],
    [ "escape", "structabigail_1_1regex_1_1escape.html", null ],
    [ "regex_t_deleter", "structabigail_1_1regex_1_1regex__t__deleter.html", null ],
    [ "row", "structabigail_1_1row.html", null ],
    [ "noop_deleter", "structabigail_1_1sptr__utils_1_1noop__deleter.html", null ],
    [ "style", "structabigail_1_1style.html", null ],
    [ "function_suppression::parameter_spec", "classabigail_1_1suppr_1_1function__suppression_1_1parameter__spec.html", null ],
    [ "function_suppression::parameter_spec::priv", "classabigail_1_1suppr_1_1function__suppression_1_1parameter__spec_1_1priv.html", null ],
    [ "function_suppression::priv", "structabigail_1_1suppr_1_1function__suppression_1_1priv.html", null ],
    [ "suppression_base", "classabigail_1_1suppr_1_1suppression__base.html", [
      [ "file_suppression", "classabigail_1_1suppr_1_1file__suppression.html", null ],
      [ "function_suppression", "classabigail_1_1suppr_1_1function__suppression.html", null ],
      [ "type_suppression", "classabigail_1_1suppr_1_1type__suppression.html", null ],
      [ "variable_suppression", "classabigail_1_1suppr_1_1variable__suppression.html", null ]
    ] ],
    [ "suppression_base::priv", "classabigail_1_1suppr_1_1suppression__base_1_1priv.html", null ],
    [ "type_suppression::insertion_range", "classabigail_1_1suppr_1_1type__suppression_1_1insertion__range.html", null ],
    [ "type_suppression::insertion_range::boundary", "classabigail_1_1suppr_1_1type__suppression_1_1insertion__range_1_1boundary.html", [
      [ "type_suppression::insertion_range::fn_call_expr_boundary", "classabigail_1_1suppr_1_1type__suppression_1_1insertion__range_1_1fn__call__expr__boundary.html", null ],
      [ "type_suppression::insertion_range::integer_boundary", "classabigail_1_1suppr_1_1type__suppression_1_1insertion__range_1_1integer__boundary.html", null ]
    ] ],
    [ "type_suppression::priv", "classabigail_1_1suppr_1_1type__suppression_1_1priv.html", null ],
    [ "variable_suppression::priv", "structabigail_1_1suppr_1_1variable__suppression_1_1priv.html", null ],
    [ "svg", "structabigail_1_1svg.html", null ],
    [ "filtered_symtab", "classabigail_1_1symtab__reader_1_1filtered__symtab.html", null ],
    [ "symtab", "classabigail_1_1symtab__reader_1_1symtab.html", null ],
    [ "symtab_filter", "classabigail_1_1symtab__reader_1_1symtab__filter.html", null ],
    [ "InOutSpec", "structabigail_1_1tests_1_1read__common_1_1_in_out_spec.html", null ],
    [ "options", "structabigail_1_1tests_1_1read__common_1_1options.html", null ],
    [ "temp_file", "classabigail_1_1tools__utils_1_1temp__file.html", null ],
    [ "timer", "classabigail_1_1tools__utils_1_1timer.html", null ],
    [ "typography", "structabigail_1_1typography.html", null ],
    [ "queue", "classabigail_1_1workers_1_1queue.html", null ],
    [ "queue::task_done_notify", "structabigail_1_1workers_1_1queue_1_1task__done__notify.html", null ],
    [ "task", "classabigail_1_1workers_1_1task.html", [
      [ "test_task", "structabigail_1_1tests_1_1read__common_1_1test__task.html", null ]
    ] ],
    [ "charDeleter", "structabigail_1_1xml_1_1char_deleter.html", null ],
    [ "textReaderDeleter", "structabigail_1_1xml_1_1text_reader_deleter.html", null ],
    [ "base_iterator", "classbase__iterator.html", [
      [ "symtab_iterator", "classabigail_1_1symtab__reader_1_1symtab__iterator.html", null ]
    ] ],
    [ "optional< bool >", "classabg__compat_1_1optional.html", null ],
    [ "optional< elf_symbols >", "classabg__compat_1_1optional.html", null ],
    [ "optional< std::string >", "classabg__compat_1_1optional.html", null ],
    [ "optional< string_elf_symbols_map_type >", "classabg__compat_1_1optional.html", null ],
    [ "optional< uint32_t >", "classabg__compat_1_1optional.html", null ],
    [ "vector", null, [
      [ "d_path_vec", "classabigail_1_1diff__utils_1_1d__path__vec.html", null ]
    ] ],
    [ "elf_symbols", "clasself__symbols.html", null ],
    [ "string_elf_symbols_map_type", "classstring__elf__symbols__map__type.html", null ],
    [ "uint32_t", "classuint32__t.html", null ]
];