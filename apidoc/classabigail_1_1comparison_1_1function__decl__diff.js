var classabigail_1_1comparison_1_1function__decl__diff =
[
    [ "priv", "structabigail_1_1comparison_1_1function__decl__diff_1_1priv.html", null ],
    [ "function_decl_diff", "classabigail_1_1comparison_1_1function__decl__diff.html#a9ee029e57a5ce224221981099411e096", null ],
    [ "chain_into_hierarchy", "classabigail_1_1comparison_1_1function__decl__diff.html#a0e317efdbadc20233996b8d205517d95", null ],
    [ "first_function_decl", "classabigail_1_1comparison_1_1function__decl__diff.html#ad0d4511189075a5c89d75c02aadf2a6e", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1function__decl__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1function__decl__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1function__decl__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1function__decl__diff.html#affab8b451a4f72363e9c750c96e64ed9", null ],
    [ "second_function_decl", "classabigail_1_1comparison_1_1function__decl__diff.html#a47b7d6f00921c7d15bde399299c131c3", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1function__decl__diff.html#a4b02f41f597cdebb0b0cc0354f4baa19", null ]
];