var classabigail_1_1comparison_1_1type__decl__diff =
[
    [ "type_decl_diff", "classabigail_1_1comparison_1_1type__decl__diff.html#abd99c999cbfde837fce1aba796d317b4", null ],
    [ "first_type_decl", "classabigail_1_1comparison_1_1type__decl__diff.html#a193becbf556ccdcdca30060c91f9987e", null ],
    [ "get_pretty_representation", "classabigail_1_1comparison_1_1type__decl__diff.html#adb9bdf9e44f9aaed311a06908c4f66c0", null ],
    [ "has_changes", "classabigail_1_1comparison_1_1type__decl__diff.html#a0477d2d9ca7eba4cca300b5d16e6f0b5", null ],
    [ "has_local_changes", "classabigail_1_1comparison_1_1type__decl__diff.html#acc62e91e11a4e064e305c3364ab37fd4", null ],
    [ "report", "classabigail_1_1comparison_1_1type__decl__diff.html#ab92bd33ba478c0a3a0587c7fe833131d", null ],
    [ "second_type_decl", "classabigail_1_1comparison_1_1type__decl__diff.html#a4ba2762165da9a155a4ab2bc37eb8b7c", null ],
    [ "compute_diff", "classabigail_1_1comparison_1_1type__decl__diff.html#aa00ccb67bca7f85588b5bbaa07659ce3", null ]
];