var structabigail_1_1comparison_1_1class__or__union__diff_1_1priv =
[
    [ "count_filtered_changed_dm", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#a3925db6109fc1d7661af5fa3a8479cce", null ],
    [ "count_filtered_changed_mem_fns", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#ac0bffad61438c65cceb146051d4145dd", null ],
    [ "count_filtered_deleted_mem_fns", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#aea2c67721d3863ab27ad07e1549794b6", null ],
    [ "count_filtered_inserted_mem_fns", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#ae82f45eb3536d46a7df6089d7d88ce55", null ],
    [ "count_filtered_subtype_changed_dm", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#a679d7119028cc34abf4b462e458eb1b3", null ],
    [ "get_deleted_non_static_data_members_number", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#a9bec2e34ecd9290a5ef9dbec9d95725a", null ],
    [ "get_inserted_non_static_data_members_number", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#af25192a07058fc3056fcb779aac96ab6", null ],
    [ "member_class_tmpl_has_changed", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#ae3f534d7e529a99a7656e1910b1cb12a", null ],
    [ "member_type_has_changed", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#ad90317fceb062734abaed9b220c5a695", null ],
    [ "subtype_changed_dm", "structabigail_1_1comparison_1_1class__or__union__diff_1_1priv.html#ac94cd1fafa4c7cf2c1569f500ca88560", null ]
];