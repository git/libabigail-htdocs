var classabigail_1_1diff__utils_1_1snake =
[
    [ "snake", "classabigail_1_1diff__utils_1_1snake.html#a16ef6926b4b52fe9b69f545ddccf5db5", null ],
    [ "snake", "classabigail_1_1diff__utils_1_1snake.html#a777ef0d9a55a2bca82b101084d39cd7e", null ],
    [ "snake", "classabigail_1_1diff__utils_1_1snake.html#ad0cb1fe47a8725712e720e4bbb0a3250", null ],
    [ "add", "classabigail_1_1diff__utils_1_1snake.html#a7ccf1f1c92bdb15ce0bf86cb1ec82d8a", null ],
    [ "begin", "classabigail_1_1diff__utils_1_1snake.html#ad3412f6c17e2cbec4658bf589f7b8300", null ],
    [ "begin", "classabigail_1_1diff__utils_1_1snake.html#a71a0ffea02a4a1f9b0391ad5403cadeb", null ],
    [ "diagonal_start", "classabigail_1_1diff__utils_1_1snake.html#a769796c0d58e0759637ce05f99d08ae8", null ],
    [ "diagonal_start", "classabigail_1_1diff__utils_1_1snake.html#a77fe297a08452445a907f06b89bd3168", null ],
    [ "end", "classabigail_1_1diff__utils_1_1snake.html#a76cf0bb495174b0fb07026ce812d294b", null ],
    [ "end", "classabigail_1_1diff__utils_1_1snake.html#a6c72c52a749ad87dafedc9536869b948", null ],
    [ "has_diagonal_edge", "classabigail_1_1diff__utils_1_1snake.html#ac228cf99942d585358dcadef621957a9", null ],
    [ "has_horizontal_edge", "classabigail_1_1diff__utils_1_1snake.html#aefed8190bf27abfbb02fcb7f8562369a", null ],
    [ "has_vertical_edge", "classabigail_1_1diff__utils_1_1snake.html#a3043bfcc201af83f0b39ed76c08d6828", null ],
    [ "intermediate", "classabigail_1_1diff__utils_1_1snake.html#a4f263dddddcb42dac0920fb7d1e2bd63", null ],
    [ "intermediate", "classabigail_1_1diff__utils_1_1snake.html#a531cb548ddab134ce9eb71325c146849", null ],
    [ "is_empty", "classabigail_1_1diff__utils_1_1snake.html#a5a2b7a58dc85678d08752945ff655362", null ],
    [ "is_forward", "classabigail_1_1diff__utils_1_1snake.html#aacc23e5912b54ffb6ad869072af41d25", null ],
    [ "set", "classabigail_1_1diff__utils_1_1snake.html#a8b66a9fc77bc0d29f8a5e8b9a370e946", null ],
    [ "set", "classabigail_1_1diff__utils_1_1snake.html#aca1ee035bbc26aaf5b1496812232b89c", null ],
    [ "set_forward", "classabigail_1_1diff__utils_1_1snake.html#a7a52bb19cc8779eac246de050e2c957b", null ]
];