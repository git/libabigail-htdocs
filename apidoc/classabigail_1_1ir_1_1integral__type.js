var classabigail_1_1ir_1_1integral__type =
[
    [ "base_type", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9", [
      [ "INT_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9ab4347e25621399ee1080daecdb8635e3", null ],
      [ "CHAR_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a9677fdd0d5c9e95c51a84b2eb240fb56", null ],
      [ "BOOL_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a7c46c1a04e91803d2a1e2f9b9a9abcd9", null ],
      [ "DOUBLE_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9ae58d3136a59c53d101bb90a6f1e6fa5b", null ],
      [ "FLOAT_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a4ba149800e38b2ab6e4da233f64bf1be", null ],
      [ "CHAR16_T_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a9c5fd6690c2f3b317116bf504b1d35e1", null ],
      [ "CHAR32_T_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a29c2248d1a6039be72517a9b3217397d", null ],
      [ "WCHAR_T_BASE_TYPE", "classabigail_1_1ir_1_1integral__type.html#a81ccc9ea96deaa049b6fd0ade6e737a9a110dc1c4ecbd426f372494ab1a52b7ca", null ]
    ] ],
    [ "modifiers_type", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555", [
      [ "NO_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555a1d6133ca0b3fb2d27f88c1590a0c3a70", null ],
      [ "SIGNED_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555a4fb04b86f19ea8b9529554ef81d20444", null ],
      [ "UNSIGNED_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555ac195f5fb3d869c0b02479281a6ed59e1", null ],
      [ "SHORT_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555ac54667e409fb5b81839bfc30f5949737", null ],
      [ "LONG_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555a4186c71cdd034403fc179bd7b1867bde", null ],
      [ "LONG_LONG_MODIFIER", "classabigail_1_1ir_1_1integral__type.html#a5a538597423de7ef9d757b38c295e555a3c659e350cd642e859b8c637305fbe37", null ]
    ] ],
    [ "integral_type", "classabigail_1_1ir_1_1integral__type.html#ac2913c3bdcc9d94c1088867601960196", null ],
    [ "integral_type", "classabigail_1_1ir_1_1integral__type.html#a27b7f0233ce173e057f5314551360be0", null ],
    [ "integral_type", "classabigail_1_1ir_1_1integral__type.html#af014980e5f4c356796ee4654e7e87fa1", null ],
    [ "get_base_type", "classabigail_1_1ir_1_1integral__type.html#a587563b5a31b75dac183565e54a05320", null ],
    [ "get_modifiers", "classabigail_1_1ir_1_1integral__type.html#aa0f9bdba6883d075ff02ea7b1e7100bc", null ],
    [ "operator string", "classabigail_1_1ir_1_1integral__type.html#a3e3a36eea4c9459a4b9ec041f4068be2", null ],
    [ "operator==", "classabigail_1_1ir_1_1integral__type.html#a692021b898e3431878b1c29dbdc6eafe", null ],
    [ "set_modifiers", "classabigail_1_1ir_1_1integral__type.html#a13a9de69155ec7c5de42e070e282714c", null ],
    [ "to_string", "classabigail_1_1ir_1_1integral__type.html#a193f20b665b7d045123810f64cfefda9", null ]
];