var classabigail_1_1ir_1_1class__or__union =
[
    [ "hash", "structabigail_1_1ir_1_1class__or__union_1_1hash.html", "structabigail_1_1ir_1_1class__or__union_1_1hash" ],
    [ "priv", "structabigail_1_1ir_1_1class__or__union_1_1priv.html", "structabigail_1_1ir_1_1class__or__union_1_1priv" ],
    [ "data_members", "classabigail_1_1ir_1_1class__or__union.html#abc4cbf807836295d03600285d8356322", null ],
    [ "member_functions", "classabigail_1_1ir_1_1class__or__union.html#a4f2e9772b5115c0839c33072831de0df", null ],
    [ "member_types", "classabigail_1_1ir_1_1class__or__union.html#ad90a86ccaf831307b51e821737587847", null ],
    [ "string_mem_fn_ptr_map_type", "classabigail_1_1ir_1_1class__or__union.html#a553225f9e00d4fcc970b70bc22784f24", null ],
    [ "string_mem_fn_sptr_map_type", "classabigail_1_1ir_1_1class__or__union.html#af73ea87e143876deacd5c97cf8af5367", null ],
    [ "virtual_mem_fn_map_type", "classabigail_1_1ir_1_1class__or__union.html#a4005cebf16e3c1e3fd3791c02727a8a2", null ],
    [ "class_or_union", "classabigail_1_1ir_1_1class__or__union.html#ab55199e90478c32ef358c67aba1630df", null ],
    [ "class_or_union", "classabigail_1_1ir_1_1class__or__union.html#a934883fdd71e208f871355dc3b8cf91f", null ],
    [ "class_or_union", "classabigail_1_1ir_1_1class__or__union.html#af1c956add6175ecc576e636d5927ff4a", null ],
    [ "~class_or_union", "classabigail_1_1ir_1_1class__or__union.html#abc5ed9cd9503da8a3bf31f4d6c186c45", null ],
    [ "add_data_member", "classabigail_1_1ir_1_1class__or__union.html#a5e48afa130f28151874c48af0139faa5", null ],
    [ "add_member_class_template", "classabigail_1_1ir_1_1class__or__union.html#aa09674bd036299244c3131c1d93f12f2", null ],
    [ "add_member_decl", "classabigail_1_1ir_1_1class__or__union.html#a9f7ddd64aa2f145020b8aa67ba92e261", null ],
    [ "add_member_function", "classabigail_1_1ir_1_1class__or__union.html#a1bda97ba2858f0494340747e07c008b0", null ],
    [ "add_member_function", "classabigail_1_1ir_1_1class__or__union.html#a69f292bdba35973008ee3aca5dbf3462", null ],
    [ "add_member_function_template", "classabigail_1_1ir_1_1class__or__union.html#a3bd0469c496df04cf538a30763af9e01", null ],
    [ "find_anonymous_data_member", "classabigail_1_1ir_1_1class__or__union.html#a22fcc70752d899dbdbc831d1fb46306a", null ],
    [ "find_data_member", "classabigail_1_1ir_1_1class__or__union.html#aef38ade1a69c133a30a786293b1d0dff", null ],
    [ "find_data_member", "classabigail_1_1ir_1_1class__or__union.html#af6341378204fe8cebf2abc0813b611b2", null ],
    [ "find_member_function", "classabigail_1_1ir_1_1class__or__union.html#a147a8947a537975deaad46d606206c97", null ],
    [ "find_member_function", "classabigail_1_1ir_1_1class__or__union.html#aad763596e6a76d3a9fb4c3e0c3be6b0a", null ],
    [ "find_member_function_from_signature", "classabigail_1_1ir_1_1class__or__union.html#a5c22e12ed4f5bf34475b50f6b3454bf1", null ],
    [ "find_member_function_from_signature", "classabigail_1_1ir_1_1class__or__union.html#a726f16281546f1aa78570fb029be7b25", null ],
    [ "find_member_function_sptr", "classabigail_1_1ir_1_1class__or__union.html#a80487cbe021a27f218cc570984748ab8", null ],
    [ "get_alignment_in_bits", "classabigail_1_1ir_1_1class__or__union.html#acdee99d049775d8a164fdd48a6240f3a", null ],
    [ "get_data_members", "classabigail_1_1ir_1_1class__or__union.html#a534588827fb4a1ff2fce312e21206193", null ],
    [ "get_member_class_templates", "classabigail_1_1ir_1_1class__or__union.html#ad395b1dfa083667603c4ac72d6f416a4", null ],
    [ "get_member_function_templates", "classabigail_1_1ir_1_1class__or__union.html#a30db38b21e55e56e12791dd3bf9d03e2", null ],
    [ "get_member_functions", "classabigail_1_1ir_1_1class__or__union.html#a25bc3e84eefe35d809a67d21ee73adc4", null ],
    [ "get_non_static_data_members", "classabigail_1_1ir_1_1class__or__union.html#aa2251003c9c1ce01b352c87119cfa973", null ],
    [ "get_num_anonymous_member_classes", "classabigail_1_1ir_1_1class__or__union.html#a06d75c9846965318908cd1d3ecac0184", null ],
    [ "get_num_anonymous_member_enums", "classabigail_1_1ir_1_1class__or__union.html#add254aa2950c5841b322461e423f13e5", null ],
    [ "get_num_anonymous_member_unions", "classabigail_1_1ir_1_1class__or__union.html#a37441f1909b48e5f91f2dde555ada090", null ],
    [ "get_size_in_bits", "classabigail_1_1ir_1_1class__or__union.html#a34f8b53f2d7ed66ad1a13cf57507d8a0", null ],
    [ "has_no_member", "classabigail_1_1ir_1_1class__or__union.html#abf47651135988297a5845bfd0b54dd32", null ],
    [ "insert_member_decl", "classabigail_1_1ir_1_1class__or__union.html#a8fbaf386c3c6b353d88ccc54582fd96a", null ],
    [ "maybe_fixup_members_of_anon_data_member", "classabigail_1_1ir_1_1class__or__union.html#ab4f416423261cd99d1f264ed145a8744", null ],
    [ "operator==", "classabigail_1_1ir_1_1class__or__union.html#a222b858dbf790da87a0b89099ff8d70c", null ],
    [ "operator==", "classabigail_1_1ir_1_1class__or__union.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1class__or__union.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "remove_member_decl", "classabigail_1_1ir_1_1class__or__union.html#a29c7f358e2fe24ce1cc0dadfc464028b", null ],
    [ "set_alignment_in_bits", "classabigail_1_1ir_1_1class__or__union.html#ad449cd23b1d282ca2d1a147a51069716", null ],
    [ "set_size_in_bits", "classabigail_1_1ir_1_1class__or__union.html#a862e0347bb3d5c1c0a17e9ef79da9929", null ],
    [ "traverse", "classabigail_1_1ir_1_1class__or__union.html#aa0435efbde239af376b5d0dfa16135d2", null ],
    [ "equals", "classabigail_1_1ir_1_1class__or__union.html#ac05ce485838445686f3f2e3285a25f18", null ],
    [ "equals", "classabigail_1_1ir_1_1class__or__union.html#af53de95726c9a5394dcb79a719a377ba", null ],
    [ "fixup_virtual_member_function", "classabigail_1_1ir_1_1class__or__union.html#a27e6ad1e71418b257c79b3bfe274b143", null ],
    [ "set_member_is_static", "classabigail_1_1ir_1_1class__or__union.html#ac80e7df10f93c869ce91f3580333393b", null ]
];