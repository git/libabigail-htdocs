var classabigail_1_1ir_1_1type__decl =
[
    [ "type_decl", "classabigail_1_1ir_1_1type__decl.html#a596535cf9ad6f5f5d904d6f5d500cc51", null ],
    [ "get_pretty_representation", "classabigail_1_1ir_1_1type__decl.html#aea2770952dab500dd4565fb7881411f3", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1type__decl.html#a48d1909c881954a9ffe4efd640625196", null ],
    [ "get_qualified_name", "classabigail_1_1ir_1_1type__decl.html#a29015ca799ed6bc508517123347e0958", null ],
    [ "operator!=", "classabigail_1_1ir_1_1type__decl.html#a3007ce6e331be979a7a92bf3d45d36dd", null ],
    [ "operator==", "classabigail_1_1ir_1_1type__decl.html#ac44702320e0a5e43c0d26926d8345284", null ],
    [ "operator==", "classabigail_1_1ir_1_1type__decl.html#a9aa1d858f09bc6864a2453d221ca45d8", null ],
    [ "operator==", "classabigail_1_1ir_1_1type__decl.html#a69962ba29b5d945547414144d7cfec16", null ],
    [ "traverse", "classabigail_1_1ir_1_1type__decl.html#a5f12d4bece91d0d613fc189bbca62113", null ]
];